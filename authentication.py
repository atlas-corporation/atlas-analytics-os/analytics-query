import constants
from database import db

god_mode = [x.lower() for x in constants.god_mode]

def has_access(access_token, data):
    if "scene" not in data:
        return False
    else:
        scene_name = data["scene"]
    if "parcels" not in data:
        return False
    else:
        parcels = data["parcels"]
    nonce = (
        db.nonces.find({"access_token": access_token}, {"_id": 0})
        .sort("nonce", -1)
        .limit(1)[0]
    )
    try:
        scenes = nonce["scenes"]
        if (
            len(
                [
                    x
                    for x in scenes
                    if x["sceneName"] == scene_name
                    if x["parcels"] == parcels
                ]
            )
            > 0
        ):
            return nonce["address"]
    except:
        return ""

def has_worlds_access(access_token, data):
    if "scene" not in data:
        return False
    else:
        scene_name = data["scene"]

    if "world" not in data:
        return False
    else:
        world = data["world"]

    if "realm" not in data:
        return False
    else:
        realm = data["realm"]
    
    nonce = (
        db.nonces.find({"access_token": access_token}, {"_id": 0})
        .sort("nonce", -1)
        .limit(1)[0]
    )
    try:
        worlds = nonce["worlds"]
        if (
            len(
                [
                    x
                    for x in worlds
                    if x["sceneName"] == scene_name
                    if x["world"] == world
                    if x["realm"] == realm
                ]
            )
            > 0
        ):
            return nonce["address"]
    except:
        return ""

def has_god_mode(address: str):
    return address.lower() in god_mode
