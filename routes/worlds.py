import csv
import datetime
import time
from collections import Counter

import dateutil
import copy
import numpy
import requests
from bson import ObjectId, json_util
from flask import Blueprint, json, jsonify, request, send_file
from flask_jwt_extended import get_jwt_identity, jwt_required

import constants
import filters
import routes
from authentication import has_worlds_access, has_god_mode
from database import db
from polygon import emote_metadata

worlds_api = Blueprint("worlds", __name__)

god_mode = [x.lower() for x in constants.god_mode]

#### JWT / Access Functions #####
@worlds_api.route("/worlds/latest-scenes", methods=["GET", "POST"])
def worlds_scenes(keep_in_house=False):
	stopwatch = time.time()
	analytics = list(db['worlds-login-cache'].aggregate([
		{
			'$match':{
				"realm":{"$nin":["localhost",""]}
			}
		},
		{
			'$project': {
				'_id':0
			}
		}
	],allowDiskUse=True,))
	print("worlds complete " + str(time.time() - stopwatch), flush=True)
	if keep_in_house:
		return analytics
	else:
		return jsonify(analytics)

# load time (ideally per scene)
@worlds_api.route("/worlds/branches", methods=["POST"])
@jwt_required()
def branches():
	req = request.json

	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	try:
		analytics = list(
			db["worlds-login-cache"].aggregate(
				[
					{
						"$match": {
							"sceneName": req["scene"],
							"world":req["world"],
							"realm":req["realm"]
						}
					}
				]
			)
		)
	except:
		return {"msg": "db call failed"}, 500
	if len(analytics) > 0:
		analytics = analytics[0]["branches"]
	return jsonify({"data": analytics})


# unique lifetime users
@worlds_api.route("/worlds/lifetime-users", methods=["POST"])
@jwt_required()
def lifetime_users():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	users = list(
		db.analytics_private.aggregate(
			[
				{
					"$match": {
						"sceneName": req["scene"],
						"world":req["world"],
						"realm":req["realm"]
					}
				},
				{"$project": {"player": 1}},
				{"$group": {"_id": None, "unique-users": {"$addToSet": "$player"}}},
				{"$project": {"_id": 0, "lifetime-users": {"$size": "$unique-users"}}},
			]
		)
	)
	return jsonify(users[0])


# unique scenes
@worlds_api.route("/worlds/event-count", methods=["POST"])
@jwt_required()
def event_count():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	event_list = list(
		db.analytics_private.aggregate(filters.matchForMongo(req) + [{"$count": "event-count"}])
	)
	return jsonify(event_list[0])


# unique scenes
@worlds_api.route("/worlds/event-types", methods=["POST"])
@jwt_required()
def events():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401
	event_list = list(
		db.analytics_private.aggregate(
			filters.matchForMongo(req)
			+ [
				{
					"$group": {
						"_id": None,
						"eventTypes": {"$addToSet": "$eventName"},
					}
				},
			]
		)
	)
	try:
		event_list = event_list[0]["eventTypes"]
		event_list = [
			x for x in event_list if x not in ["ping", "load-timer", "idle", "birthday"]
		]
	except:
		event_list = []
	return jsonify(event_list)

# load time (ideally per scene)
@worlds_api.route("/worlds/load-time", methods=["GET", "POST"])
@jwt_required()
def load_time():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401
	try:
		analytics = list(
			db.analytics_private.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$match": {"eventName": "load-timer"}},
					{
						"$addFields": {
							"load-time": {
								"$subtract": ["$data.endTime", "$data.startTime"]
							}
						}
					},
					{"$sort": {"data.startTime": 1}},
					{"$project": {"_id": 0, "timestamp": 1, "load-time": 1}},
				]
			)
		)
	except:
		return {"msg": "db call failed"}, 500

	loadtimes = [
		[datetime.datetime.fromtimestamp(x["timestamp"] / 1000), x["load-time"] / 1000]
		for x in analytics
	]
	return jsonify({"data": [["timestamp", "load-time"]] + loadtimes})


@worlds_api.route("/worlds/load-time-histogram", methods=["GET", "POST"])
@jwt_required()
def load_time_histogram():
	if request.method == "POST":
		req = request.json
	else:
		req = None

	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	try:
		analytics = list(
			db.analytics_private.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$match": {"eventName": "load-timer"}},
					{
						"$addFields": {
							"load-time": {
								"$subtract": [
									"$data.sceneInitData.endTime",
									"$data.sceneInitData.startTime"
								]
							}
						}
					},
					{
						"$lookup": {
							"from": "userAgents", 
							"localField": "userAgent",
							"foreignField": "_id", 
							"as": "userAgentDetails"
						}
					},
					{"$unwind": "$userAgentDetails"}, # Deconstructs the "userAgentDetails" array
					{"$project": {"_id": 0, "load-time": 1, "deviceType": "$userAgentDetails.device.type"}},
					{
							"$group": {
								"_id": {
									"timeRange": {
										"$switch": {
											"branches": [
												{"case": {"$lt": ["$load-time", 2000]}, "then": "0-2 seconds"},
												{"case": {"$and": [{"$gte": ["$load-time", 2000]}, {"$lt": ["$load-time", 5000]}]}, "then": "2-5 seconds"},
												{"case": {"$and": [{"$gte": ["$load-time", 5000]}, {"$lt": ["$load-time", 10000]}]}, "then": "5-10 seconds"},
											],
											"default": "10+ seconds"
										}
									},
									"deviceType": "$deviceType"
								},
								"userCount": {"$sum": 1}
							}
						},
						{
							"$project": {
								"_id": 0,
								"timeRange": "$_id.timeRange",
								"userCount": 1,
								"deviceType": "$_id.deviceType"
							}
						}
				]
			)
		)
	except:
		return {"msg": "db call failed"}, 500


	return jsonify({'data':analytics})


###### USER DATA ############
@worlds_api.route("/worlds/guests-vs-users", methods=["GET", "POST"])
@jwt_required()
def guests_vs_users():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401
	try:
		user_data = list(
			db.analytics_private.aggregate(
				filters.matchForMongo(req)
				+ [  
					{"$match": {"eventName": "ping"}},
					{"$project": {"player": 1, "guest": 1}},
					{
						"$group": {
							"_id": {
								"$cond": [{"$eq": ["$guest", True]}, "Guest", "Wallet"]
							},
							"players": {"$addToSet": "$player"},
						}
					},
					{"$addFields": {"count": {"$size": "$players"}}},
					{
						"$project": {
							"players": 0,
						}
					},
				]
			)
		)

	except:
		return {"msg": "db call failed"}, 500

	try:
		if len([x for x in user_data if x["_id"] == "Wallet"]) == 0:
			user_data = [{"_id": "Wallet", "count": 0}] + user_data
		if len([x for x in user_data if x["_id"] == "Guest"]) == 0:
			user_data.append({"_id": "Guest", "count": 0})

	except:
		pass

	return jsonify({"data": user_data})


@worlds_api.route("/worlds/wallets-over-time", methods=["POST"])
@jwt_required()
def wallets_over_time():
	req = request.json

	### Scene Acess control ###
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	if not ("start" in req and "end" in req):
		return {"msg": "invalid request"}, 500

	start = dateutil.parser.isoparse(req["start"])
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "hour"
	if dt.total_seconds() >= 259200:
		unit = "day"
	try:
		analytics = list(
			db.analytics_private.aggregate(
				filters.matchForMongo(req)
				+ [
					{
						"$group": {
							"_id": {
								"$dateTrunc": {
									"date": {"$toDate": "$timestamp"},
									"unit": unit,
									"timezone": filters.timezoneOffset(req),
									"binSize": 1,
								}
							},
							"wallets": {"$addToSet": "$player"},
						}
					},
					{"$addFields": {"count": {"$size": "$wallets"}}},
					{"$sort": {"_id": 1}},
				]
				+ filters.include_users(req)
			)
		)
		analytics = filters.pad_zeroes(
			req, analytics, bin=3600 if unit == "hour" else 86400
		)
	except:
		return {"msg": "db call failed"}, 500
	return jsonify({"data": analytics})

@worlds_api.route("/worlds/wallets-over-time/summary", methods=["POST"])
@jwt_required()
def wallets_over_time_summary():
	req = request.json

	### Scene Acess control ###
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	if not (req["start"] and req["end"]):
		return {"msg": "invalid request"}, 500

	start = dateutil.parser.isoparse(req["start"])
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "hour"
	if dt.total_seconds() >= 259200:
		unit = "day"
	try:
		analytics = list(
			db.analytics_private.aggregate(
				filters.matchForMongo(req)
				+ [
					{
						"$group": {
							"_id": None,
							"wallets": {"$addToSet": "$player"},
						}
					},
					{"$addFields": {"count": {"$size": "$wallets"}}},
				]
				+ filters.include_users(req)
			)
		)
	except:
		return {"msg": "db call failed"}, 500

	count = 0
	if len(analytics):
		count = analytics[0]["count"]
	return jsonify({"data": {"count": count}})


@worlds_api.route("/worlds/unique-users", methods=["POST"])
@jwt_required()
def unique_users():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	### Scene Acess control ###
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401
	try:
		analytics = list(
			db.analytics_private.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$project": {"player": 1}},
					{
						"$group": {
							"_id": "unique-users",
							"wallets": {"$addToSet": "$player"},
						}
					},
					{"$addFields": {"count": {"$size": "$wallets"}}},
					{"$sort": {"_id": 1}},
				]
				+ filters.include_users(req)
			)
		)
	except:
		return {"msg": "db call failed"}, 500
	return jsonify({"data": analytics[0]})

@worlds_api.route("/worlds/users-by-realm", methods=["POST"])
@jwt_required()
def users_by_realm(keep_in_house=False):
	req = request.json
	worlds_realms= {
		"worlds-server.atlascorp.io/content/world/":"atlas-worlds",		
		"worlds.decentral.games/content/world/":"decentral-games-worlds",
		"worlds.dcl-iwb.co/world/":"dcl-in-world-builder",
		".dcl.eth":"foundation-worlds"
	}
	### Scene Acess control ###
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	start = dateutil.parser.isoparse(req["start"])
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "hour"
	if dt.total_seconds() >= 259200:
		unit = "day"
	try:
		analytics = list(
			db.analytics_private.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$project": {"player": 1, "realm": 1, "timestamp": 1}},
					{
						"$group": {
							"_id": {
								"date": {
									"$dateTrunc": {
										"date": {"$toDate": "$timestamp"},
										"unit": unit,
										"timezone": filters.timezoneOffset(req),
										"binSize": 1,
									}
								},
								"realm": "$realm",
							},
							"wallets": {"$addToSet": "$player"},
						}
					},
					{"$addFields": {"count": {"$size": "$wallets"}}},
					{
						"$group": {
							"_id": "$_id.date",
							"realms": {"$push": {"k": "$_id.realm", "v": "$count"}},
							"totalCount": {"$sum": "$count"},
						}
					},
					{
						"$project": {
							"realmData": {"$arrayToObject": "$realms"},
							"totalCount": 1,
						}
					},
					{"$replaceWith": {"$mergeObjects": ["$$ROOT", "$realmData"]}},
					{"$project": {"realmData": 0}},
					{"$sort": {"_id": 1}},
				]
				+ filters.include_users(req)
			)
		)
	except:
		return {"msg": "db call failed"}, 500
	analytics = filters.pad_zeroes_realms(
		req, analytics, bin=3600 if unit == "hour" else 86400
	)

	events = []
	sum = 0
	for i in analytics:
		if "totalCount" in i:
			sum += i["totalCount"]
		
		#other worlds based on dict above
		for key in worlds_realms:
			for field in i.keys():
				if key in field:
					i[worlds_realms[key]] = i.pop(field)
					break

		events.append({**i, "cumulative": sum})


	if keep_in_house:
		return events
	else:
		return jsonify({"data": events})


@worlds_api.route("/worlds/wallets-by-height", methods=["POST"])
@jwt_required()
def wallets_by_height():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	### Scene Acess control ###
	if not has_worlds_access(request.headers['Authorization'].split(" ")[1],req):
		return {"msg":"User does not have access to world"},401

	analytics = list(
		db.analytics_private.aggregate(
			filters.matchForMongo(req)
			+ [
				{"$project": {"timestamp": 1, "player": 1, "playerPosition": 1}},
				{
					"$group": {
						"_id": {
							"date": {
								"$dateTrunc": {
									"date": {"$toDate": "$timestamp"},
									"unit": "second",
									"timezone": filters.timezoneOffset(req),
									"binSize": filters.bin_size(req),
								}
							},
							"height": {"$floor": "$playerPosition.y"},
						},
						"wallets": {"$addToSet": "$player"},
					}
				},
				{"$addFields": {"count": {"$size": "$wallets"}}},
				{
					"$group": {
						"_id": "$_id.date",
						"height": {
							"$push": {"k": {"$toString": "$_id.height"}, "v": "$count"}
						},
					}
				},
				{"$project": {"height": {"$arrayToObject": "$height"}}},
				{"$replaceWith": {"$mergeObjects": ["$$ROOT", "$height"]}},
				{"$project": {"height": 0}},
				{"$sort": {"_id": 1}},
			]
			+ filters.include_users(req)
		)
	)
	analytics = filters.pad_zeroes(req, analytics)
	return jsonify({"data": analytics})

# user agents
@worlds_api.route("/worlds/scene-user-agents", methods=["POST"])
@jwt_required()
def scene_user_agents():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	analytics = list(
		db.analytics_private.aggregate(
			filters.matchForMongo(req)
			+ [
				{"$project": {"userAgent": 1, "player": 1}},
				{"$group": {"_id": "$userAgent", "players": {"$addToSet": "$player"}}},
				{"$addFields": {"count": {"$size": "$players"}}},
			]
		)
	)

	for u in analytics:
		try:
			ua_data = list(db.userAgents.find({"_id": u["_id"]}))[0]
			u["os"] = ua_data["os"]["family"]
			u["client"] = ua_data["type"]
			u["browser"] = ua_data["browser"]["name"]
			u["device"] = ua_data["device"]["name"]
			u["device-type"] = ua_data["device"]["type"]
		except:
			pass
	data = {"os": [], "client": [], "browser": [], "device": [], "device-type": []}
	for a in list(set([x["os"] for x in analytics if "os" in x])):
		data["os"].append(
			{
				"_id": a,
				"count": sum(
					[
						len(users["players"])
						for users in analytics
						if "os" in users
						if users["os"] == a
					]
				),
			}
		)
	for b in list(set([x["client"] for x in analytics if "client" in x])):
		data["client"].append(
			{
				"_id": b,
				"count": sum(
					[
						len(users["players"])
						for users in analytics
						if "client" in users
						if users["client"] == b
					]
				),
			}
		)
	for c in list(set([x["browser"] for x in analytics if "browser" in x])):
		data["browser"].append(
			{
				"_id": c,
				"count": sum(
					[
						len(users["players"])
						for users in analytics
						if "browser" in users
						if users["browser"] == c
					]
				),
			}
		)
	for d in list(set([x["device"] for x in analytics if "device" in x])):
		data["device"].append(
			{
				"_id": d,
				"count": sum(
					[
						len(users["players"])
						for users in analytics
						if "device" in users
						if users["device"] == d
					]
				),
			}
		)
	for e in list(set([x["device-type"] for x in analytics if "device-type" in x])):
		data["device-type"].append(
			{
				"_id": e,
				"count": sum(
					[
						len(users["players"])
						for users in analytics
						if "device-type" in users
						if users["device-type"] == e
					]
				),
			}
		)

	return jsonify(data)


######### EVENT & EMOTE QUERIES ###############
# for chart of events per day (line, bar) - [date, cumulative sum, daily sum]
@worlds_api.route("/worlds/events-per-day/<event_type>", methods=["GET", "POST"])
@jwt_required()
def events_per_day(event_type):
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401
	start = dateutil.parser.isoparse(req["start"])
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "hour"
	if dt.total_seconds() >= 259200:
		unit = "day"
	try:
		analytics = list(
			db.analytics_private.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$match": {"eventName": event_type}},
					{
						"$group": {
							"_id": {
								"$dateTrunc": {
									"date": {"$toDate": "$timestamp"},
									"unit": unit,
									"timezone": filters.timezoneOffset(req),
									"binSize": 1,
								}
							},
							"count": {"$sum": 1},
						}
					},
					{"$sort": {"_id": 1}},
				]
			)
		)
		# cumulative
		analytics = filters.pad_zeroes(
			req, analytics, bin=3600 if unit == "hour" else 86400
		)
		events = []
		sum = 0
		for i in analytics:
			sum += i["count"]
			events.append({"_id": i["_id"], "count": i["count"], "cumulative": sum})
	except Exception as e:
		print(e)
		return {"msg": "db call failed"}, 500
	return jsonify({"data": events})

@worlds_api.route("/worlds/emote-trends", methods=["GET", "POST"])
@jwt_required()
def emote_trends():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401
	start = dateutil.parser.isoparse(req["start"])
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "hour"
	if dt.total_seconds() >= 259200:
		unit = "day"

	analytics = list(
		db.analytics_private.aggregate(
			filters.matchForMongo(req)
			+ [
				{"$match": {"eventName": "player-animation"}},
				{
					"$group": {
						"_id": {
							"emoji":"$data.expression",
							"date":{"$dateTrunc": {
								"date": {"$toDate": "$timestamp"},
								"unit": unit,
								"timezone": filters.timezoneOffset(req),
								"binSize": 1,
							}
						}},
						"count": {"$sum": 1},
					}
				},
				{
					"$group": {
						"_id": "$_id.date",
						"emojis": {
							"$push": {
								"k": "$_id.emoji",  
								"v": "$count"
							}
						}
					}
				},
				{
					"$addFields": {
						"emojis": {
							"$arrayToObject": "$emojis"
						}
					}
				},
				{
					"$replaceRoot": {
						"newRoot": {
							"$mergeObjects": [{ "_id": "$_id" }, "$emojis"]
						}
					}
				},				  
				{"$sort": {"_id": 1,}},
			]
		)
	)
	analytics = filters.pad_zeroes(
		req, analytics, bin=3600 if unit == "hour" else 86400
	)

	return jsonify({"data": analytics})

@worlds_api.route("/worlds/emotes", methods=["GET", "POST"])
@jwt_required()
def emotes():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401
	start = dateutil.parser.isoparse(req["start"])
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "hour"
	if dt.total_seconds() >= 259200:
		unit = "day"
	try:
		analytics = list(
			db.analytics_private.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$match": {"eventName": "player-animation"}},
					{
						"$group": {
							"_id": "$data.expression",
							"count": {"$sum": 1}
						}
					},
					{
						"$project": {
							"_id": 0,  
							"emote": "$_id",
							"count": "$count"
						}
					},				   
					{"$sort": {"_id": 1}}
				]
			)
		)

	except Exception as e:
		print(e)
		return {"msg": "db call failed"}, 500
	
	#emote resolution
	for a in analytics:
		if 'urn:decentraland:matic:collections-v2:' in a['emote']:
			# the contract returns a tuple, the 5th element contains meta data with the 3rd item being name
			a['emote'] = emote_metadata(a['emote'])[5].split(":")[2] 

	return jsonify({"data": analytics})

######### MULTIPLE EVENTS QUERIES ###############
# for chart of events per day (line, bar) - [date, cumulative sum, daily sum]
@worlds_api.route("/worlds/events-per-day", methods=["POST"])
@jwt_required()
def events_per_day_multiple():
	req = request.json
	if "events" not in req:
		return {"msg": "Invalid API Call"}, 500

	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	start = dateutil.parser.isoparse(req["start"])
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "hour"
	if dt.total_seconds() >= 259200:
		unit = "day"
	try:
		analytics = list(
			db.analytics_private.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$match": {"eventName": {"$in": req["events"]}}},
					{
						"$group": {
							"_id": {
								"date": {
									"$dateTrunc": {
										"date": {"$toDate": "$timestamp"},
										"unit": unit,
										"timezone": filters.timezoneOffset(req),
										"binSize": 1,
									}
								},
								"eventName": "$eventName",
							},
							"count": {"$sum": 1},
						}
					},
					{
						"$group": {
							"_id": "$_id.date",
							"events": {
								"$push": {
									"k": "$_id.eventName",
									"v": "$count",
								}
							},
							"totalCount": {"$sum": "$count"},
						}
					},
					{
						"$project": {
							"events": {"$arrayToObject": "$events"},
							"totalCount": 1,
						}
					},
					{"$replaceWith": {"$mergeObjects": ["$$ROOT", "$events"]}},
					{"$project": {"events": 0}},
					{"$sort": {"_id": 1}},
				]
			)
		)
		# cumulative
		analytics = filters.pad_zeroes_realms(
			req, analytics, bin=3600 if unit == "hour" else 86400
		)
		events = []
		sum = 0
		for i in analytics:
			if "totalCount" in i:
				sum += i["totalCount"]
			events.append({**i, "cumulative": sum})
	except Exception as e:
		print(e)
		return {"msg": "db call failed"}, 500
	return jsonify({"data": events})

# events by unique users
@worlds_api.route("/worlds/user-events/<event_type>", methods=["GET", "POST"])
@jwt_required()
def user_events(event_type):
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401
	try:
		analytics = list(
			db.analytics_private.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$match": {"eventName": event_type}},
					{
						"$group": {
							"_id": {"address": "$player", "ip": "$ip"},
							"count": {"$sum": 1},
						}
					},
					{"$sort": {"_id": 1}},
				]
			)
		)
		# cumulative
		events = []
		sum = 0
		for i in analytics:
			sum += i["count"]
			events.append([i["_id"], sum, i["count"]])
	except:
		return {"msg": "db call failed"}, 500
	return jsonify({"data": events})


# events by unique user
@worlds_api.route("/worlds/user-history/<user>", methods=["GET", "POST"])
@worlds_api.route("/worlds/user-history/<user>/<page>", methods=["GET", "POST"])
@jwt_required()
def user_history(user,page=None):
	if page:
		skip = int(page) * 50
		pagination = [{"$skip": skip},{"$limit": 50}]	
	else:
		pagination = []
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401
	try:
		analytics = list(	
			db.analytics_private.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$match": {"player": user.lower(),"eventName":{"$ne":"ping"}}},
					{"$project":{'player':1,'playerPosition':1,'parcel':1,'eventName':1,'timestamp':1,'data':{'$ifNull':['$data',None]},'sceneInitData': {"$ifNull": ["$sceneInitData", None]},'_id':0}},
					{"$sort": {"timestamp": 1}},
					{
						'$facet': {
							'totalCount': [{'$count': 'count'}],
							'data': pagination
						}
					},
					{
						'$addFields': {
							'totalCount': {
								'$arrayElemAt': ['$totalCount.count', 0]
							}
						}
					}
					]
				)
			)
	except:
		return {"msg": "DB call failed"}, 500
	emote_lookup = {}
	analytics = analytics[0]
	for a in analytics['data']:
		if 'data' in a:
			if a['data']:
				if 'expression' in a['data']:
					emote = a['data']['expression']
					if 'urn:decentraland:matic:collections-v2:' in emote:
						# the contract returns a tuple, the 5th element contains meta data with the 3rd item being name
						if emote not in emote_lookup:
							a['data']['expression'] = emote_metadata(key)[5].split(":")[2]
						emote_lookup[emote] = emote_name
				if 'sceneInitData' in a['data']:
					if a['data']['sceneInitData']:
						a['loaddata'] = {
							"load-time": float(a['data']['sceneInitData']['endTime']) - float(a['data']['sceneInitData']['startTime']),
							"platform":a['data']['sceneInitData']['platform'],
							"parcels":a['data']['sceneInitData']['parcels'],
							"playerName":a['data']['playerName']
						}
					del a['data']
	return jsonify(analytics)

@worlds_api.route("/worlds/time-on-site", methods=["GET", "POST"])
@jwt_required()
def time_on_site():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers['Authorization'].split(" ")[1],req):
		return {"msg":"User does not have access to world"},401
	
	analytics = list(
		db.analytics_private.aggregate(
			filters.matchForMongo(req)
			+ [
				{"$match": {"eventName": "ping"}},
				{
					"$group": {
						"_id": {
							"date": {
								"$dateTrunc": {
									"date": {"$toDate": "$timestamp"},
									"unit": "second",
									"binSize": filters.bin_size(req),
								}
							},
							"user": "$auth",
						},
						"time-on-site": {"$sum": 5},
					}
				},
				{"$sort": {"_id": 1}},
			]
			+ filters.include_users(req)
		)
	)

	analytics = [
		{
			"date": a["_id"]["date"],
			"user": a["_id"]["user"],
			"time-on-site": a["time-on-site"],
		}
		for a in analytics
	]
	with open("timeonsite.csv", "w") as f:
		dict_writer = csv.DictWriter(f, ["date", "user", "time-on-site"])
		dict_writer.writeheader()
		dict_writer.writerows(analytics)

	return send_file("./timeonsite.csv", mimetype="text/csv")

@worlds_api.route("/worlds/by-wallet/<event_type>", methods=["GET", "POST"])
@jwt_required()
def event_by_wallet(event_type):
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	# scene ping rate
	try:
		scene_parcels = [[str(x[0]) + "," + str(x[1])] for x in req["parcels"]]
		pollingInterval = list(
			db.analytics_private.find(
				{
					"eventName": "load-timer",
					"sceneName": req["scene"],
					"data.sceneInitData.parcels": scene_parcels,
				}
			)
			.sort("timestamp", -1)
			.limit(1)
		)[0]["data"]["sceneInitData"]["pollingInterval"]
		ping_multiplier = pollingInterval / 5000
	except:
		ping_multiplier = 1  # 5 second is default, so 1 * 5
	try:
		analytics = db.analytics_private.aggregate(
			filters.matchForMongo(req)
			+ [
				{"$match": {"eventName": event_type}},
				{"$group": {"_id": "$player", "count": {"$sum": 1 * ping_multiplier}}}
			]
		)
		analytics = list(analytics)
	except:
		return {"msg": "db call failed"}, 500

	users = list(set([str(x["_id"]) for x in analytics]))

	# cumulative
	user_data = list(
		db.users.aggregate(
			[{"$match": {"address": {"$in": users}, "guest": {"$ne": None}}}]
		)
	)
	for a in analytics:
		try:
			user = [x for x in user_data if x["address"] == a["_id"]][0]
			a["address"] = a["_id"]
			# DCL Data
			a["dclName"] = user["avatar"]["avatar"]["name"]
			a["avatar"] = user["avatar"]["avatar"]["face"]
			#replace old URLs-legacy fix
			a["avatar"] = a["avatar"].replace("peer.kyllian.me","catalyst.atlascorp.io")
			a["avatar"] = a["avatar"].replace("peer-wc1.decentraland.org","catalyst.atlascorp.io")
			a["avatar"] = a["avatar"].replace("peer-eu1.decentraland.org","catalyst.atlascorp.io")
			a["avatar"] = a["avatar"].replace("peer-ec1.decentraland.org","catalyst.atlascorp.io")
			a["avatar"] = a["avatar"].replace("interconnected.online","catalyst.atlascorp.io")
			a["guest"] = user["guest"]
		except:
			a["address"] = a["_id"]

		try:
			user = {}
			user = [x for x in user_data if x["address"] == a["_id"]][0]
			a["guest"] = user["guest"]
		except:
			if "avatar" not in user:
				a["guest"] = True
			else:
				if "avatar" not in user["avatar"]:
					a["guest"] = True
				else:
					a["guest"] = False

		del a["_id"]


	return jsonify({"data": analytics})

@worlds_api.route("/worlds/summary/<event_type>", methods=["POST"])
@jwt_required()
def event_summary(event_type):
	req = request.json

	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	try:
		pollingInterval = list(
			db['worlds-login-cache'].find(
				{
					"sceneName": req["scene"],
					"world": req["world"],
					"realm": req["realm"],
				}
			)
			.limit(1)
		)[0]["pollingInterval"]
		ping_multiplier = pollingInterval / 5000
	except Exception as e:
		print(e)
		ping_multiplier = 1  # 5 second is default, so 1 * 5
	try:
		analytics = db.analytics_private.aggregate(
			filters.matchForMongo(req)
			+ [
				{"$match": {"eventName": event_type}},
				{"$group": {"_id": None, "count": {"$sum": 1 * ping_multiplier}}},
			]
		)
		analytics = list(analytics)
	except:
		return {"msg": "db call failed"}, 500

	count = 0
	if len(analytics):
		count = analytics[0]["count"]

	return jsonify({"data": {"count": count}})

@worlds_api.route("/worlds/summary/<event_type>/average", methods=["POST"])
@jwt_required()
def event_summary_average(event_type):
	req = request.json

	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	# scene ping rate
	try:
		pollingInterval = list(
			db['worlds-login-cache'].find(
				{
					"sceneName": req["scene"],
					"world": req["world"],
					"realm": req["realm"],
				}
			)
			.limit(1)
		)[0]["pollingInterval"]
		ping_multiplier = pollingInterval / 5000
	except Exception as e:
		print(e)
		ping_multiplier = 1  # 5 second is default, so 1 * 5
	try:
		analytics = db.analytics_private.aggregate(
			filters.matchForMongo(req)
			+ [
				{"$match": {"eventName": event_type}},
				{"$group": {"_id": "$player", "count": {"$sum": 1 * ping_multiplier}}},
				{"$group": {"_id": None, "count": {"$avg": "$count"}}},
			]
		)
		analytics = list(analytics)
	except:
		return {"msg": "db call failed"}, 500

	count = 0
	if len(analytics):
		count = analytics[0]["count"]

	return jsonify({"data": {"count": count}})

######### GEOGRAPHY QUERIES ###############
# number of users per geo who performed an event at least once
@worlds_api.route("/worlds/events-by-geo/<event_type>", methods=["GET", "POST"])
@worlds_api.route(
	"/worlds/events-by-geo/<event_type>/<country>", methods=["GET", "POST"]
)
@worlds_api.route(
	"/worlds/events-by-geo/<event_type>/<country>/<granularity>", methods=["GET", "POST"]
)
@jwt_required()
def user_events_by_geo(event_type, country="United States", granularity=False):
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401
	try:
		analytics = list(
			db.analytics_private.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$match": {"eventName": event_type}},
					{"$group": {"_id": "$ip", "count": {"$sum": 1}}},
					{"$sort": {"_id": 1}},
				]
			)
		)

	except:
		return {"msg": "db call failed"}, 500
	users = [x["_id"] for x in analytics]

	if not granularity or granularity == "country":
		event_users = db.users.aggregate(
			[
				{"$match": {"ip": {"$in": users}, "location.country": country}},
				{"$group": {"_id": "$location.country", "count": {"$sum": 1}}},
			]
		)
		event_users = list(event_users)
	elif granularity == "region":
		event_users = db.users.aggregate(
			[
				{"$match": {"ip": {"$in": users}, "location.country": country}},
				{
					"$group": {
						"_id": "$location.regionName",
						"country": {"$addToSet": "$location.country"},
						"count": {"$sum": 1},
					}
				},
			]
		)
		event_users = list(event_users)
		for e in event_users:
			e["country"] = e["country"][0]

	elif granularity == "city":
		event_users = db.users.aggregate(
			[
				{"$match": {"ip": {"$in": users}, "location.country": country}},
				{
					"$group": {
						"_id": "$location.city",
						"country": {"$addToSet": "$location.country"},
						"region": {"$addToSet": "$location.region"},
						"count": {"$sum": 1},
					}
				},
			]
		)
		event_users = list(event_users)
		for e in event_users:
			e["region"] = e["region"][0]
			e["country"] = e["country"][0]

	if not event_users:
		return ""
	else:
		keys = event_users[0].keys()
		with open("event_by_geo.csv", "w", newline="") as output_file:
			dict_writer = csv.DictWriter(output_file, keys)
			dict_writer.writeheader()
			dict_writer.writerows(event_users)

		return send_file("./event_by_geo.csv", mimetype="text/csv")

# unique wallets (ETH addresses) by geo
@worlds_api.route("/worlds/wallets-by-geo", methods=["GET", "POST"])
@worlds_api.route("/worlds/wallets-by-geo/<country>", methods=["GET", "POST"])
@worlds_api.route(
	"/worlds/wallets-by-geo/<country>/<granularity>", methods=["GET", "POST"]
)
@jwt_required()
def wallets_by_geo(country="United States", granularity=False):
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401
	try:
		if country == "*":
			country = {"$ne" : None}
		analytics = list(
			db.analytics_private.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$group": {"_id": "$player", "count": {"$sum": 1}}},
					{"$sort": {"_id": 1}},
				]
			)
		)
	except:
		return {"msg": "db call failed"}, 500
	users = [x["_id"] for x in analytics if x["_id"]]
	if not granularity:
		event_users = db.users.aggregate(
			[
				{"$match": {"address": {"$in": users}}},
				{
					"$group": {
						"_id": None,
						"unique-wallets": {
							"$addToSet": {
								"address": "$address",
								"country": "$location.country",
							}
						},
					}
				},
				{"$unwind": "$unique-wallets"},
				{"$project": {"_id": 0}},
				{"$group": {"_id": "$unique-wallets.country", "count": {"$sum": 1}}},
			]
		)
		event_users = list(event_users)
	if granularity == "country":
		event_users = db.users.aggregate(
			[
				{"$match": {"address": {"$in": users}, "location.country": country}},
				{
					"$group": {
						"_id": None,
						"unique-wallets": {
							"$addToSet": {
								"address": "$address",
								"country": "$location.country",
							}
						},
					}
				},
				{"$unwind": "$unique-wallets"},
				{"$project": {"_id": 0}},
				{"$group": {"_id": "$unique-wallets.country", "count": {"$sum": 1}}},
			]
		)
		event_users = list(event_users)
	elif granularity == "region":
		event_users = db.users.aggregate(
			[
				{"$match": {"address": {"$in": users}, "location.country": country}},
				{
					"$group": {
						"_id": None,
						"unique-wallets": {
							"$addToSet": {
								"address": "$address",
								"country": "$location.country",
								"regionName": "$location.regionName",
							}
						},
					}
				},
				{"$unwind": "$unique-wallets"},
				{"$project": {"_id": 0}},
				{
					"$group": {
						"_id": "$unique-wallets.regionName",
						"country": {"$addToSet": "$unique-wallets.country"},
						"count": {"$sum": 1},
					}
				}
			]
		)
		event_users = list(event_users)
		response = {}
		for e in event_users:
			if e["country"][0] not in response:
				response[e["country"][0]] = {}
			response[e["country"][0]][e["_id"]] = e["count"]

	elif granularity == "city":
		event_users = db.users.aggregate(
			[
				{"$match": {"address": {"$in": users}, "location.country": country}},
				{
					"$group": {
						"_id": None,
						"unique-wallets": {
							"$addToSet": {
								"address": "$address",
								"country": "$location.country",
								"region": "$location.regionName",
								"city": "$location.city",
							}
						},
					}
				},
				{"$unwind": "$unique-wallets"},
				{"$project": {"_id": 0}},
				{
					"$group": {
						"_id": "$unique-wallets.city",
						"country": {"$addToSet": "$unique-wallets.country"},
						"region": {"$addToSet": "$unique-wallets.region"},
						"count": {"$sum": 1},
					}
				}			  
			]
		)
		event_users = list(event_users)
		response = {}
		for e in event_users:
			if e["country"][0] not in response:
				response[e["country"][0]] = {}
			if e["region"][0] not in response[e["country"][0]]:
				response[e["country"][0]][e["region"][0]] = {}
			response[e["country"][0]][e["region"][0]][e["_id"]] = e["count"]

	if not event_users:
		return None
	else:
		return jsonify({'data': response})

@worlds_api.route("/worlds/world-heat-map", methods=["GET", "POST"])
@jwt_required()
def world_heat_map():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	event_users = list(
		db.analytics_private.aggregate(
			filters.matchForMongo(req)
			+ [
				{"$project": {"_id": 0, "player": 1, "auth": 1, "ip": 1}},
				{"$group": {"_id": "$ip"}},
				{
					"$lookup": {
						"from": "users",
						"localField": "_id",
						"foreignField": "ip",
						"as": "location",
					}
				},
				{"$project": {"location": {"$first": "$location.location.country"}}},
				{"$group": {"_id": "$location", "count": {"$sum": 1}}},
				{"$project": {"name": "$_id", "count": 1, "_id": 0}},
			]
		)
	)

	for u in event_users:
		if u["name"] is None:
			u["code"] = None
		else:
			u["code"] = constants.country_codes[u["name"]]

	maxCount = 0
	if len(event_users):
		maxCount = max([x["count"] for x in event_users])

	return jsonify({"data": event_users, "max": maxCount})


@worlds_api.route("/worlds/redundant-users", methods=["GET", "POST"])
@jwt_required()
def redundant_users():
	if request.method == "POST":
		req = request.json
	else:
		req = None

	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	options = [
		"address",
		"ip",
		"userAgent",
		"realm",
		"guest",
	] 
	if "filters" in req:
		choices = req["filters"]
	else:
		choices = []
	not_selected = list(set(options) - set(choices))
	group_function = {"$group": {"_id": {}}}
	counters = [{"$addFields": {"address-count": {"$size": "$player"}}}]

	for c in choices:
		# it's player not address in the db
		if c == "address":
			c = "player"
		if c in ["country", "region", "city"]:
			group_function["$group"]["_id"][c] = "$" + str(c)
		else:
			group_function["$group"]["_id"][c] = "$" + str(c)
	for n in not_selected:
		if n == "address":
			n = "player"
		else:
			counters.append(
				{"$addFields": {str(n) + "-count": {"$size": "$" + str(n)}}}
			)
		group_function["$group"][n] = {"$addToSet": "$" + str(n)}

	analytics = list(
		db.analytics_private.aggregate(
			filters.matchForMongo(req)  # fix
			+ [group_function]
			+ counters
			+ [
				{
					"$project": {
						"ip": 0,
						"userAgent": 0,
						"realm": 0,
						"guest": 0,
						"country": 0,
						"region": 0,
						"city": 0,
					}
				}
			]
			+ [{"$sort": {"address-count": -1}}]
		)
	)

	# user information
	for a in analytics:
		if "ip" in a["_id"]:
			a["_id"]["ip"] = "..." + str(a["_id"]["ip"].split(".")[-1])
		if "userAgent" in a["_id"]:
			a["_id"]["userAgent"] = "..." + a["_id"]["userAgent"][-10:-1]
		culprits = []
		for p in a["player"]:
			try:
				username = list(db.users.find({"address": p}).limit(1))[0]
				culprits.append(
					{
						"address": p,
						"name": username["avatar"]["avatar"]["name"],
						"face": username["avatar"]["avatar"]["face"],
					}
				)
			except:
				culprits.append({"address": p, "name": "Guest", "face": ""})
			a["culprits"] = culprits
	return jsonify({"data": analytics})

@worlds_api.route("/worlds/error-logs", methods=["POST"])
@jwt_required()
def getErrorLogs():
	req = request.json

	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	try:
		data = list(
			db.analytics_private.aggregate(
				filters.matchForMongo(req)
				+ [{"$match": {"eventName": "error-log"}}]
				+ [
					{
						"$addFields": {
							"error": "$data.logData.error",
							"level": "$data.logData.level",
						}
					}
				]
				+ [{"$project": {"ip": 0}}]
			)
		)
	except Exception as e:
		print(e)
		pass

	return json_util.dumps(data), 200


@worlds_api.route("/worlds/error-logs/users", methods=["POST"])
@jwt_required()
def getErrorLogsUsers():
	req = request.json
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401
	match = filters.matchForMongo(req)[0]["$match"]

	match.pop("timestamp", None)

	data = db.analytics_private.find_one({**match, "eventName": "error-log"})

	usedErrorLogs = True if data else False
	return {"usedErrorLogs": usedErrorLogs}, 200

@worlds_api.route("/worlds/retention/<retention_period>", methods=["GET", "POST"])
@jwt_required()
def retention(retention_period=7):
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to world"}, 401

	date_range = [dateutil.parser.isoparse(req["start"]) + datetime.timedelta(days=x) for x in range(0, (dateutil.parser.isoparse(req["end"])-dateutil.parser.isoparse(req["start"])).days+1)]
	response = {"data":[]}
	for d in date_range:
		analytics = db.analytics_private.aggregate(
			[
				{
				"$match": { 
					'sceneName': req['scene'], 
					'realm': req['realm'],
					'world': req['world'],
					'timestamp': {
						'$lt': d.timestamp() * 1000, 
						'$gte': (d - datetime.timedelta(days=1)).timestamp() * 1000
						}
					}
				},
				{"$addFields":{"date": {"$dateTrunc": {"date": { "$toDate": "$timestamp" },"unit": "day"}}}},
				{"$group":{"_id": {"player": "$player","date": "$date"}}},

				{"$lookup":{
					"from": "analytics_private", 
					"let": { "player_id": "$_id.player", "current_date": "$_id.date" },
					"pipeline": [
						{
							"$match": { 
								'sceneName': req['scene'],
								'realm': req['realm'], 
								'world': req['world'],
								'timestamp': {
									'$lt': (d - datetime.timedelta(days=1)).timestamp() * 1000, 
									'$gte': (d - datetime.timedelta(days=int(retention_period) + 1)).timestamp() * 1000
									}
								
							}
						},
					{
						"$match": {
						"$expr": {
							"$and": [
							{ "$eq": ["$player", "$$player_id"] },
							{ 
								"$gte": [
								{ "$dateTrunc": { "date": { "$toDate": "$timestamp" }, "unit": "day" } }, 
								{ "$dateSubtract": { "startDate": "$$current_date", "unit": "day", "amount": int(retention_period) } }
								] 
							},
							{ 
								"$lt": [
								{ "$dateTrunc": { "date": { "$toDate": "$timestamp" }, "unit": "day" } }, 
								"$$current_date"
								] 
							}
							]
						} 
						}
					},
					{"$project": { "_id": 0,"player":1,"timestamp":1} } 
					],
					"as": "previous_activities"
				}},
				{"$project":
					{
						"player": "$_id.player",
						"date": "$_id.date",
						"retained": {
						"$cond": { "if": { "$gt": [{ "$size": "$previous_activities" }, 0] }, "then": True, "else": False }
						}
					}}
			]
		)
		analytics = list(analytics)
		data = {"date":d,"retention":len([x for x in analytics if x["retained"]]),"players":[x['player'] for x in analytics if x['retained']]}
		if data["retention"] > 0:
			data["retention_%"] =  data["retention"]/len(analytics)
		else:	
			data["retention_%"] = 0
		response['data'].append(data)

	return jsonify(response)

@worlds_api.route("/worlds/stickiness", methods=["POST"])
@jwt_required()
def stickiness():
	req = request.json

	### Scene Acess control ###
	if not has_worlds_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401

	if not (req["start"] and req["end"]):
		return {"msg": "invalid request"}, 500

	start = dateutil.parser.isoparse(req["start"]) # Extend back one month to get MAUs
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "day"
	analytics = []

	#Rolling MAUs
	current_date = start
	while current_date <= end:
		pipeline = filters.matchForMongo(req,False) +[
			
			{
				"$match": {
					"timestamp": {"$gte": (current_date - datetime.timedelta(days=30)).timestamp() * 1000, "$lt": (current_date + datetime.timedelta(days=1)).timestamp() * 1000}
				}
			},
			{
				"$group": {
					"_id": None,
					"uniqueUsers": {"$addToSet": "$player"}
				}
			},
			{
				"$project": {
					"date": current_date,
					"MAU": {"$size": "$uniqueUsers"}
				}
			}
		]

		# Calculate MAUs
		maus = list(db.analytics.aggregate(pipeline))
		mau_count = maus[0]['MAU'] if maus else 0

		# Calculate DAUs
		dau_pipeline = filters.matchForMongo(req,True) + [
			{
				"$match": {
					"timestamp": {
						"$gte": current_date.timestamp() * 1000,
						"$lt": (current_date + datetime.timedelta(days=1)).timestamp() * 1000
					}
				}
			},
			{
				"$group": {
					"_id": None,
					"uniqueUsers": {"$addToSet": "$player"}
				}
			},
			{
				"$project": {
					"DAU": {"$size": "$uniqueUsers"}
				}
			}
		]
		daus = list(db.analytics.aggregate(dau_pipeline))
		dau_count = daus[0]['DAU'] if daus else 0

		# Calculate Stickiness
		stickiness = dau_count / mau_count if mau_count > 0 else 0

		analytics.append({"date": current_date.strftime("%Y-%m-%d"), "DAU": dau_count, "MAU": mau_count, "stickiness": stickiness})

		# Increment day
		current_date += datetime.timedelta(days=1)

	return jsonify({"data": analytics})
