import time
from uuid import uuid4

import dateutil
from bson import json_util
from flask import Blueprint, jsonify, request
from flask_jwt_extended import get_jwt_identity, jwt_required

import constants
import filters
from database import db_hyperfy

hyperfy_api = Blueprint("hyperfy", __name__)


@hyperfy_api.route("/hyperfy/scenes")
@jwt_required()
def getHyperfyScenes():
    data = list(
        db_hyperfy.deployments.aggregate(
            [
                {"$project": {"entities": 0, "_id": False}},
                {"$sort": {"timestamp": -1}},
                {"$group": {"_id": "$id", "latest": {"$first": "$$ROOT"}}},
                {"$replaceRoot": {"newRoot": "$latest"}},
                {"$sort": {"id": 1}},
            ],
            allowDiskUse=True,
        )
    )
    return data, 200


@hyperfy_api.route("/hyperfy/traffic", methods=["POST"])
@jwt_required()
def hyperfyTraffic():
    req = request.json
    data = list(
        db_hyperfy.traffic.aggregate(
            [
                {
                    "$match": {
                        "timestamp": {
                            "$gte": dateutil.parser.isoparse(req["start"]).timestamp()
                            * 1000
                        },
                    }
                },
                {
                    "$group": {
                        "_id": {
                            "id": "$id",
                            "date": {
                                "$dateTrunc": {
                                    "date": "$_id",
                                    "unit": "hour",
                                    "binSize": 1,
                                }
                            },
                        },
                        "avg": {"$avg": "$clients"},
                    }
                },
                {"$group": {"_id": "$_id.date", "count": {"$sum": "$avg"}}},
                {"$sort": {"_id": 1}},
            ]
        )
    )

    return data, 200


@hyperfy_api.route("/hyperfy/traffic/<id>", methods=["POST"])
@jwt_required()
def hyperfyTrafficPerWorld(id):
    req = request.json
    data = list(
        db_hyperfy.traffic.aggregate(
            [
                {
                    "$match": {
                        "id": id,
                        "timestamp": {
                            "$gte": dateutil.parser.isoparse(req["start"]).timestamp()
                            * 1000
                        },
                    }
                },
                {
                    "$group": {
                        "_id": {
                            "$dateTrunc": {
                                "date": {"$toDate": "$timestamp"},
                                "unit": "hour",
                                "binSize": 1,
                            },
                        },
                        "count": {"$avg": "$clients"},
                    }
                },
                {"$sort": {"_id": 1}},
            ]
        )
    )
    req["zero-padded"] = True
    data = filters.pad_zeroes(req, data, 3600)

    return data, 200


@hyperfy_api.route("/hyperfy/deployments", methods=["POST"])
@jwt_required()
def hyperfyDeployments():
    req = request.json

    data = list(
        db_hyperfy.deployments.aggregate(
            [
                {
                    "$group": {
                        "_id": {
                            "$dateTrunc": {
                                "date": {"$toDate": "$timestamp"},
                                "unit": "day",
                                "binSize": 1,
                                "timezone": filters.timezoneOffset(req),
                            }
                        },
                        "count": {"$count": {}},
                    }
                },
                {"$sort": {"_id": 1}},
            ]
        )
    )

    return data, 200


@hyperfy_api.route("/hyperfy/deployments/<id>", methods=["POST"])
@jwt_required()
def hyperfyDeploymentsPerWorld(id):
    req = request.json

    data = list(
        db_hyperfy.deployments.aggregate(
            [
                {
                    "$match": {
                        "id": id,
                    }
                },
                {
                    "$group": {
                        "_id": {
                            "$dateTrunc": {
                                "date": {"$toDate": "$timestamp"},
                                "unit": "day",
                                "binSize": 1,
                                "timezone": filters.timezoneOffset(req),
                            }
                        },
                        "count": {"$count": {}},
                    }
                },
                {"$sort": {"_id": 1}},
            ]
        )
    )
    return data, 200


@hyperfy_api.route("/hyperfy/api-generator", methods=["POST"])
@jwt_required()
def createHyperfyAPIKey():
    req = request.json

    if (
        not isinstance(req, dict)
        or "label" not in req
        or "collaborators" not in req
        or "reason" not in req
    ):
        return jsonify({"error": "Missing or invalid request body"}), 400

    if (
        not isinstance(req["label"], str)
        or not isinstance(req["collaborators"], list)
        or req["reason"] not in ["business", "personal"]
    ):
        return (
            jsonify({"error": "Invalid values for label, reason, or collaborators"}),
            400,
        )

    auth_header = request.headers.get("Authorization")
    if not auth_header:
        return jsonify({"error": "Missing Authorization header"}), 401

    address = get_jwt_identity().get("address")

    if len(req["collaborators"]) + 1 != len(
        set([address.lower(), *[col.lower() for col in req["collaborators"]]])
    ):
        return jsonify({"error": "Duplicate addresses in collaborators list"}), 400

    uuid = uuid4()
    schema = {
        "apikey": str(uuid),
        "created": int(time.time() * 1000),
        "active": True,
        "label": req["label"],
        "reason": req["reason"],
    }

    insertArr = [
        {**schema, "role": "owner", "address": address.lower()},
        *[
            {**schema, "role": "collaborator", "address": address.lower()}
            for address in req["collaborators"]
        ],
    ]

    db_hyperfy.api.insert_many(insertArr)

    return "recieved", 200


@hyperfy_api.route("/hyperfy/fetch-api")
@jwt_required()
def findHyperfyAPIKeys():
    auth_header = request.headers.get("Authorization")
    if not auth_header:
        return jsonify({"error": "Missing Authorization header"}), 401

    address = get_jwt_identity().get("address")

    data = list(db_hyperfy.api.find({"address": address.lower()}, {"_id": 0}))

    return data, 200


@hyperfy_api.route("/hyperfy/user-scenes")
@jwt_required()
def getHyperfyUserScenes():
    auth_header = request.headers.get("Authorization")
    if not auth_header:
        return jsonify({"error": "Missing Authorization header"}), 401

    address = get_jwt_identity().get("address")

    if not address:
        return 400, "unauthorized"

    api_keys = list(
        db_hyperfy.api.find(
            filter={"address": address.lower()},
            projection={"apikey": 1, "label": 1, "_id": 0},
        )
    )

    data = list(
        db_hyperfy.pings.aggregate(
            [
                {
                    "$match": {
                        "apikey": {"$in": [apikey.get("apikey") for apikey in api_keys]}
                    }
                },
                {"$group": {"_id": {"worldId": "$worldId", "apikey": "$apikey"}}},
                {
                    "$lookup": {
                        "from": "deployments",
                        "let": {"worldId": "$_id.worldId"},
                        "pipeline": [
                            {"$match": {"$expr": {"$eq": ["$id", "$$worldId"]}}},
                            {"$project": {"entities": 0, "_id": 0}},
                            {"$sort": {"timestamp": -1}},
                            {"$limit": 1},
                        ],
                        "as": "data",
                    }
                },
                {"$unwind": {"path": "$data", "preserveNullAndEmptyArrays": True}},
                {
                    "$project": {
                        "data": {"$mergeObjects": ["$data", {"apikey": "$_id.apikey"}]},
                    }
                },
                {"$replaceRoot": {"newRoot": "$data"}},
            ]
        )
    )

    return data, 200


@hyperfy_api.route("/hyperfy/wallets-over-time", methods=["POST"])
@jwt_required()
def hyperfy_wallets_over_time():
    req = request.json

    ### Scene Acess control ###

    if not ("start" in req and "end" in req and "worldId" in req and "apikey" in req):
        return {"msg": "invalid request"}, 500

    identity = get_jwt_identity()

    yeet = list(
        db_hyperfy.api.find(
            {"address": identity.get("address").lower(), "apikey": req.get("apikey")},
            {"_id": 0},
        )
    )

    if not len(yeet):
        return {"msg": "token not authorized"}, 400

    start = dateutil.parser.isoparse(req["start"])
    end = dateutil.parser.isoparse(req["end"])
    dt = end - start

    unit = "hour"
    if dt.total_seconds() >= 259200:
        unit = "day"
    try:
        analytics = list(
            db_hyperfy.pings.aggregate(
                filters.matchForMongoHyperfy(req)
                + [
                    {
                        "$group": {
                            "_id": {
                                "$dateTrunc": {
                                    "date": {"$toDate": "$timestamp"},
                                    "unit": unit,
                                    "timezone": filters.timezoneOffset(req),
                                    "binSize": 1,
                                }
                            },
                            "wallets": {
                                "$addToSet": {
                                    "walletAddress": "$walletAddress",
                                    "ipAddress": "$ipAddress",
                                }
                            },
                        }
                    },
                    {"$addFields": {"count": {"$size": "$wallets"}}},
                    {"$sort": {"_id": 1}},
                ]
                + filters.include_users(req)
            )
        )
        analytics = filters.pad_zeroes(
            req, analytics, bin=3600 if unit == "hour" else 86400
        )
        # print(analytics, flush=True)
    except:
        return {"msg": "DB call failed"}, 500
    return jsonify({"data": analytics})


@hyperfy_api.route("/hyperfy/wallets-over-time/summary", methods=["POST"])
@jwt_required()
def hyperfy_wallets_over_time_summary():
    req = request.json

    if not ("start" in req and "end" in req and "worldId" in req and "apikey" in req):
        return {"msg": "invalid request"}, 500

    identity = get_jwt_identity()

    yeet = list(
        db_hyperfy.api.find(
            {"address": identity.get("address").lower(), "apikey": req.get("apikey")},
            {"_id": 0},
        )
    )

    if not len(yeet):
        return {"msg": "token not authorized"}, 400

    start = dateutil.parser.isoparse(req["start"])
    end = dateutil.parser.isoparse(req["end"])
    dt = end - start
    unit = "hour"
    if dt.total_seconds() >= 259200:
        unit = "day"
    # try:
    analytics = list(
        db_hyperfy.pings.aggregate(
            filters.matchForMongoHyperfy(req)
            + [
                {
                    "$group": {
                        "_id": None,
                        "wallets": {
                            "$addToSet": {
                                "walletAddress": "$walletAddress",
                                "ipAddress": "$ipAddress",
                            }
                        },
                    }
                },
                {"$addFields": {"count": {"$size": "$wallets"}}},
            ]
            + filters.include_users(req)
        )
    )

    # print(analytics, flush=True)
    # except:
    #     return {"msg": "DB call failed"}, 500

    count = 0
    if len(analytics):
        count = analytics[0]["count"]
    return jsonify({"data": {"count": count}})


@hyperfy_api.route("/hyperfy/summary/<event_type>/average", methods=["POST"])
@jwt_required()
def hyperfy_event_summary_average(event_type):
    req = request.json
    if not ("start" in req and "end" in req and "worldId" in req and "apikey" in req):
        return {"msg": "invalid request"}, 500

    identity = get_jwt_identity()

    yeet = list(
        db_hyperfy.api.find(
            {"address": identity.get("address").lower(), "apikey": req.get("apikey")},
            {"_id": 0},
        )
    )

    if not len(yeet):
        return {"msg": "token not authorized"}, 400

    try:
        pollingInterval = 5000
        ping_multiplier = pollingInterval / 5000
    except Exception as e:
        print(e)
        ping_multiplier = 1  # 5 second is default, so 1 * 5
    # try:
    analytics = db_hyperfy.pings.aggregate(
        filters.matchForMongoHyperfy(req)
        + [
            {
                "$group": {
                    "_id": {
                        "walletAddress": "$walletAddress",
                        "ipAddress": "$ipAddress",
                    },
                    "count": {"$sum": 1 * ping_multiplier},
                }
            },
            {"$group": {"_id": None, "count": {"$avg": "$count"}}},
        ]
    )
    analytics = list(analytics)
    # except:
    #     return {"msg": "DB call failed"}, 500

    count = 0
    if len(analytics):
        count = analytics[0]["count"]

    return jsonify({"data": {"count": count}})


@hyperfy_api.route("/hyperfy/summary/<event_type>", methods=["POST"])
@jwt_required()
def hyperfy_event_summary(event_type):
    req = request.json
    if not ("start" in req and "end" in req and "worldId" in req and "apikey" in req):
        return {"msg": "invalid request"}, 500

    identity = get_jwt_identity()

    yeet = list(
        db_hyperfy.api.find(
            {"address": identity.get("address").lower(), "apikey": req.get("apikey")},
            {"_id": 0},
        )
    )

    if not len(yeet):
        return {"msg": "token not authorized"}, 400

    try:
        ping_multiplier = 1
    except Exception as e:
        print(e)
        ping_multiplier = 1  # 5 second is default, so 1 * 5
    try:
        analytics = db_hyperfy.pings.aggregate(
            filters.matchForMongoHyperfy(req)
            + [
                {"$group": {"_id": None, "count": {"$sum": 1 * ping_multiplier}}},
            ]
        )
        analytics = list(analytics)
    except:
        return {"msg": "DB call failed"}, 500

    count = 0
    if len(analytics):
        count = analytics[0]["count"]

    return jsonify({"data": {"count": count}})


@hyperfy_api.route("/hyperfy-world-heat-map", methods=["POST"])
@jwt_required()
def hyperfy_world_heat_map():
    req = request.json

    # if not ("start" in req and "end" in req and "worldId" in req and "apikey" in req):
    #     return {"msg": "invalid request"}, 500

    identity = get_jwt_identity()

    yeet = list(
        db_hyperfy.api.find(
            {"address": identity.get("address").lower(), "apikey": req.get("apikey")},
            {"_id": 0},
        )
    )

    if not len(yeet):
        return {"msg": "token not authorized"}, 400
    # try:
    print("findiing countries")
    event_users = list(
        db_hyperfy.pings.aggregate(
            filters.matchForMongoHyperfy(req)
            + [
                {
                    "$group": {
                        "_id": {
                            "ipAddress": "$ipAddress",
                            "user": {
                                "$cond": {
                                    "if": "$walletAddress",
                                    "then": "$walletAddress",
                                    "else": "$sessionId",
                                }
                            },
                        }
                    }
                },
                {
                    "$lookup": {
                        "from": "users",
                        "let": {"ipAddress": "$_id.ipAddress", "user": "$_id.user"},
                        "pipeline": [
                            {
                                "$match": {
                                    "$expr": {
                                        "$or": [
                                            {"$eq": ["$wallet", "$$user"]},
                                            {"$eq": ["$sessionId", "$$user"]},
                                        ]
                                    }
                                }
                            }
                        ],
                        "as": "location",
                    }
                },
                {"$project": {"location": {"$first": "$location.location.country"}}},
                {"$match": {"location": {"$exists": True}}},
                {"$group": {"_id": "$location", "count": {"$count": {}}}},
                {"$project": {"name": "$_id", "count": 1, "_id": 0}},
            ]
        )
    )
    for u in event_users:
        if u["name"] is None:
            u["code"] = None
        else:
            u["code"] = constants.country_codes[u["name"]]

    maxCount = 0
    if len(event_users):
        maxCount = max([x["count"] for x in event_users])

    return jsonify({"data": event_users, "max": maxCount})


@hyperfy_api.route("/hyperfy-event-types", methods=["POST"])
@jwt_required()
def hyperfy_event_types():
    req = request.json

    identity = get_jwt_identity()

    yeet = list(
        db_hyperfy.api.find(
            {"address": identity.get("address").lower(), "apikey": req.get("apikey")},
            {"_id": 0},
        )
    )

    if not len(yeet):
        return {"msg": "token not authorized"}, 400
    # try:
    print("findiing countries")
    event_users = list(
        db_hyperfy["distinct-events"].aggregate(
            filters.matchForMongoHyperfy(req) + [{"$group": {"_id": "$eventName"}}]
        )
    )

    data = [x["_id"] for x in event_users]

    return jsonify(data)


@hyperfy_api.route("/hyperfy-events-per-day", methods=["POST"])
@jwt_required()
def hyperfy_events_per_day():
    req = request.json

    if "events" not in req:
        return {"msg": "Invalid API Call"}, 500

    address = get_jwt_identity().get("address")

    yeet = list(
        db_hyperfy.api.find(
            {"address": address.lower(), "apikey": req.get("apikey")},
            {"_id": 0},
        )
    )
    if not len(yeet):
        return {"msg": "token not authorized"}, 400

    start = dateutil.parser.isoparse(req["start"])
    end = dateutil.parser.isoparse(req["end"])
    dt = end - start
    unit = "hour"
    if dt.total_seconds() >= 259200:
        unit = "day"
    try:
        analytics = list(
            db_hyperfy["distinct-events"].aggregate(
                filters.matchForMongoHyperfy(req)
                + [
                    {"$match": {"eventName": {"$in": req["events"]}}},
                    {
                        "$group": {
                            "_id": {
                                "date": {
                                    "$dateTrunc": {
                                        "date": {"$toDate": "$timestamp"},
                                        "unit": unit,
                                        "binSize": 1,
                                        "timezone": filters.timezoneOffset(req),
                                    }
                                },
                                "eventName": "$eventName",
                            },
                            "count": {"$count": {}},
                        }
                    },
                    {
                        "$group": {
                            "_id": "$_id.date",
                            "events": {"$push": {"k": "$_id.eventName", "v": "$count"}},
                            "totalCount": {"$sum": "$count"},
                        }
                    },
                    {
                        "$project": {
                            "events": {"$arrayToObject": "$events"},
                            "totalCount": 1,
                        }
                    },
                    {"$replaceWith": {"$mergeObjects": ["$$ROOT", "$events"]}},
                    {"$project": {"events": 0}},
                    {"$sort": {"_id": 1}},
                ]
            )
        )
        analytics = filters.pad_zeroes_realms(
            req, analytics, bin=3600 if unit == "hour" else 86400
        )
        events = []
        sum = 0
        for i in analytics:
            if "totalCount" in i:
                sum += i["totalCount"]
            events.append({**i, "cumulative": sum})
    except Exception as e:
        print(e)
        return {"msg": "DB call failed"}, 500
    return jsonify({"data": events}), 200


@hyperfy_api.route("/hyperfy-error-logs", methods=["POST"])
@jwt_required()
def hyperfy_getErrorLogs():
    req = request.json

    identity = get_jwt_identity()

    yeet = list(
        db_hyperfy.api.find(
            {"address": identity.get("address").lower(), "apikey": req.get("apikey")},
            {"_id": 0},
        )
    )

    if not len(yeet):
        return {"msg": "token not authorized"}, 400
    # try:

    data = list(
        db_hyperfy["logs"].aggregate(
            filters.matchForMongoHyperfy(req)
            + [
                {
                    "$addFields": {
                        "error": "$data",
                    }
                },
                {"$project": {"ip": 0, "userAgent": 0}},
            ]
        )
    )

    return json_util.dumps(data), 200
