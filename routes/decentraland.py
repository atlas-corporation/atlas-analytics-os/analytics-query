import csv
import datetime
import time
from collections import Counter

import dateutil
import copy
import numpy
import requests
from bson import ObjectId, json_util
from flask import Blueprint, json, jsonify, request, send_file
from flask_jwt_extended import get_jwt_identity, jwt_required

import constants
import filters
import routes
from authentication import has_access, has_god_mode
from database import db
from polygon import emote_metadata

decentraland_api = Blueprint("decentraland", __name__)

god_mode = [x.lower() for x in constants.god_mode]

# Genesis City was the original so it's the main routes, but also need standardization to /genesis-city/

@decentraland_api.route("/genesis-city/branches", methods=["POST"])
@decentraland_api.route("/branches", methods=["POST"])
@jwt_required()
def branches():
	req = request.json

	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401
	print( [str(p[0]) + "," + str(p[1]) for p in req["parcels"]],flush=True)
	try:
		analytics = list(
			db["login-cache"].aggregate(
				[
					{
						"$match": {
							"sceneName": req["scene"],
							"parcels": [str(p[0]) + "," + str(p[1]) for p in req["parcels"]],
						}
					}
				]
			)
		)
	except:
		return {"msg": "DB call failed"}, 500
	if len(analytics) > 0:
		try:
			analytics = analytics[0]["branches"]
		except:
			analytics = []
	return jsonify({"data": analytics})

@decentraland_api.route("/genesis-city/has-avatar/<address>")
@decentraland_api.route("/has-avatar/<address>")
def has_avatar(address, keep_in_house=False):
	try:
		profile = requests.get(
			filters.get_server() + "/lambdas/profiles/?id=" + str(address).lower()
		).json()
		avatar = {
			"avatar": {
				"name": profile[0]["avatars"][0]["name"],
				"face": profile[0]["avatars"][0]["avatar"]["snapshots"]["face256"],
			}
		}
	except:
		avatar = {"hasAvatar": False}
	if keep_in_house:
		return avatar
	else:
		return jsonify(avatar)

# unique lifetime users
@decentraland_api.route("/genesis-city'/lifetime-users", methods=["POST"])
@decentraland_api.route("/lifetime-users", methods=["POST"])
@jwt_required()
def lifetime_users():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401

	users = list(
		db.analytics.aggregate(
			[
				{
					"$match": {
						"sceneName": req["scene"],
						"parcel": {"$in": req["parcels"]},
					}
				},
				{"$project": {"player": 1}},
				{"$group": {"_id": None, "unique-users": {"$addToSet": "$player"}}},
				{"$project": {"_id": 0, "lifetime-users": {"$size": "$unique-users"}}},
			]
		)
	)
	return jsonify(users[0])

# unique event types for dropdowns
@decentraland_api.route("/genesis-city/event-types", methods=["POST"])
@decentraland_api.route("/event-types", methods=["POST"])
@jwt_required()
def events():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401
	event_list = list(
		db.analytics.aggregate(
			filters.matchForMongo(req)
			+ [
				{
					"$group": {
						"_id": None,
						"eventTypes": {"$addToSet": "$eventName"},
					}
				},
			]
		)
	)
	try:
		event_list = event_list[0]["eventTypes"]
		event_list = [
			x for x in event_list if x not in ["ping", "load-timer", "idle", "birthday"]
		]
	except:
		event_list = []
	return jsonify(event_list)

@decentraland_api.route("/genesis-city/load-time-histogram", methods=["GET", "POST"])
@decentraland_api.route("/load-time-histogram", methods=["GET", "POST"])
@jwt_required()
def load_time_histogram():
	if request.method == "POST":
		req = request.json
	else:
		req = None

	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401

	try:
		analytics = list(
			db.analytics.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$match": {"eventName": "load-timer"}},
					{
						"$addFields": {
							"load-time": {
								"$subtract": [
									"$data.sceneInitData.endTime",
									"$data.sceneInitData.startTime"
								]
							}
						}
					},
					{
						"$lookup": {
							"from": "userAgents", 
							"localField": "userAgent", 
							"foreignField": "_id",
							"as": "userAgentDetails" 
						}
					},
					{"$unwind": "$userAgentDetails"},
					{"$project": {"_id": 0, "load-time": 1, "deviceType": "$userAgentDetails.device.type"}},
					{
							"$group": {
								"_id": {
									"timeRange": {
										"$switch": {
											"branches": [
												{"case": {"$lt": ["$load-time", 2000]}, "then": "0-2 seconds"},
												{"case": {"$and": [{"$gte": ["$load-time", 2000]}, {"$lt": ["$load-time", 5000]}]}, "then": "2-5 seconds"},
												{"case": {"$and": [{"$gte": ["$load-time", 5000]}, {"$lt": ["$load-time", 10000]}]}, "then": "5-10 seconds"},
											],
											"default": "10+ seconds"
										}
									},
									"deviceType": "$deviceType"
								},
								"userCount": {"$sum": 1}
							}
						},
						{
							"$project": {
								"_id": 0,
								"timeRange": "$_id.timeRange",
								"userCount": 1,
								"deviceType": "$_id.deviceType"
							}
						}
				]
			)
		)
	except:
		return {"msg": "DB call failed"}, 500

	return jsonify({'data':analytics})

###### USER DATA ############
# Signed fetch replacement - caution, not locked down
@decentraland_api.route("/signed-fetch/<address>/<x>/<y>/<since>", methods=["GET"])  # in world last n minutes
@decentraland_api.route("/signed-fetch/<address>/<x>/<y>/<since>/<tolerance>", methods=["GET"])  
def signed_fetch(address, x, y, since, tolerance=None):
	try:
		# get all data points from user in last N minutes
		analytics = list(
			db.analytics.find(
				{
					"timestamp": {"$gte": (time.time() * 1000) - (int(since) * 1000)},
					"player": str(address.lower()),
				},
				{"_id": 0},
			))
		is_present = False
		for a in analytics:
			if tolerance:
				#  d = (x2 − x1)^2 + (y2 − y1)^2
				distance = ((float(a["playerPosition"]["x"]) / 16) - float(x)) ** 2 + (
					(float(a["playerPosition"]["z"]) / 16) - float(y)
				) ** 2
				if distance <= float(tolerance):
					is_present = True
					break
			else:
				if a["parcel"] == [int(x), int(y)]:
					is_present = True
					break
	except:
		return {"msg":"DB call failed"},500

	return jsonify({"is-present": is_present})

@decentraland_api.route("/genesis-city/guests-vs-users", methods=["GET", "POST"])
@decentraland_api.route("/guests-vs-users", methods=["GET", "POST"])
@jwt_required()
def guests_vs_users():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401
	try:
		user_data = list(
			db.analytics.aggregate(
				filters.matchForMongo(req)
				+ [  # fix
					{"$match": {"eventName": "ping"}},
					{"$project": {"player": 1, "guest": 1}},
					{
						"$group": {
							"_id": {
								"$cond": [{"$eq": ["$guest", True]}, "Guest", "Wallet"]
							},
							"players": {"$addToSet": "$player"},
						}
					},
					{"$addFields": {"count": {"$size": "$players"}}},
					{
						"$project": {
							"players": 0,
						}
					},
				]
			)
		)

	except:
		return {"msg": "DB call failed"}, 500

	try:
		if len([x for x in user_data if x["_id"] == "Wallet"]) == 0:
			user_data = [{"_id": "Wallet", "count": 0}] + user_data
		if len([x for x in user_data if x["_id"] == "Guest"]) == 0:
			user_data.append({"_id": "Guest", "count": 0})

	except:
		pass

	return jsonify({"data": user_data})

@decentraland_api.route("/genesis-city/wallets-over-time", methods=["POST"])
@decentraland_api.route("/wallets-over-time", methods=["POST"])
@jwt_required()
def wallets_over_time():
	req = request.json

	### Scene Acess control ###
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401

	if not ("start" in req and "end" in req):
		return {"msg": "invalid request"}, 500

	start = dateutil.parser.isoparse(req["start"])
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "hour"
	if dt.total_seconds() >= 259200:
		unit = "day"
	try:
		analytics = list(
			db.analytics.aggregate(
				filters.matchForMongo(req)
				+ [
					{
						"$group": {
							"_id": {
								"$dateTrunc": {
									"date": {"$toDate": "$timestamp"},
									"unit": unit,
									"timezone": filters.timezoneOffset(req),
									"binSize": 1,
								}
							},
							"wallets": {"$addToSet": "$player"},
						}
					},
					{"$addFields": {"count": {"$size": "$wallets"}}},
					{"$sort": {"_id": 1}},
				]
				+ filters.include_users(req)
			)
		)
		analytics = filters.pad_zeroes(
			req, analytics, bin=3600 if unit == "hour" else 86400
		)
		# print(analytics, flush=True)
	except:
		return {"msg": "DB call failed"}, 500
	return jsonify({"data": analytics})

@decentraland_api.route("/genesis-city/wallets-over-time/summary", methods=["POST"])
@decentraland_api.route("/wallets-over-time/summary", methods=["POST"])
@jwt_required()
def wallets_over_time_summary():
	req = request.json

	### Scene Acess control ###
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401

	if not (req["start"] and req["end"]):
		return {"msg": "invalid request"}, 500

	start = dateutil.parser.isoparse(req["start"])
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "hour"
	if dt.total_seconds() >= 259200:
		unit = "day"
	try:
		analytics = list(
			db.analytics.aggregate(
				filters.matchForMongo(req)
				+ [
					{
						"$group": {
							"_id": None,
							"wallets": {"$addToSet": "$player"},
						}
					},
					{"$addFields": {"count": {"$size": "$wallets"}}},
				]
				+ filters.include_users(req)
			)
		)

		# print(analytics, flush=True)
	except:
		return {"msg": "DB call failed"}, 500

	count = 0
	if len(analytics):
		count = analytics[0]["count"]
	return jsonify({"data": {"count": count}})

@decentraland_api.route("/genesis-city/users-by-realm", methods=["POST"])
@decentraland_api.route("/users-by-realm", methods=["POST"])
@jwt_required()
def users_by_realm(keep_in_house=False):
	req = request.json

	### Scene Acess control ###
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401

	start = dateutil.parser.isoparse(req["start"])
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "hour"
	if dt.total_seconds() >= 259200:
		unit = "day"
	try:
		analytics = list(
			db.analytics.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$project": {"player": 1, "realm": 1, "timestamp": 1}},
					{
						"$group": {
							"_id": {
								"date": {
									"$dateTrunc": {
										"date": {"$toDate": "$timestamp"},
										"unit": unit,
										"timezone": filters.timezoneOffset(req),
										"binSize": 1,
									}
								},
								"realm": "$realm",
							},
							"wallets": {"$addToSet": "$player"},
						}
					},
					{"$addFields": {"count": {"$size": "$wallets"}}},
					{
						"$group": {
							"_id": "$_id.date",
							"realms": {"$push": {"k": "$_id.realm", "v": "$count"}},
							"totalCount": {"$sum": "$count"},
						}
					},
					{
						"$project": {
							"realmData": {"$arrayToObject": "$realms"},
							"totalCount": 1,
						}
					},
					{"$replaceWith": {"$mergeObjects": ["$$ROOT", "$realmData"]}},
					{"$project": {"realmData": 0}},
					{"$sort": {"_id": 1}},
				]
				+ filters.include_users(req)
			)
		)
	except:
		return {"msg": "DB call failed"}, 500
	analytics = filters.pad_zeroes_realms(
		req, analytics, bin=3600 if unit == "hour" else 86400
	)

	events = []
	sum = 0
	for i in analytics:
		if "totalCount" in i:
			sum += i["totalCount"]
		events.append({**i, "cumulative": sum})

	if keep_in_house:
		return analytics
	else:
		return jsonify({"data": events})

# user agents
@decentraland_api.route("/genesis-city/scene-user-agents", methods=["POST"])
@decentraland_api.route("/scene-user-agents", methods=["POST"])
@jwt_required()
def scene_user_agents():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401

	analytics = list(
		db.analytics.aggregate(
			filters.matchForMongo(req)
			+ [
				{"$project": {"userAgent": 1, "player": 1}},
				{"$group": {"_id": "$userAgent", "players": {"$addToSet": "$player"}}},
				{"$addFields": {"count": {"$size": "$players"}}},
			]
		)
	)
	# print(analytics, flush=True)
	for u in analytics:
		try:
			ua_data = list(db.userAgents.find({"_id": u["_id"]}))[0]
			u["os"] = ua_data["os"]["family"]
			u["client"] = ua_data["type"]
			u["browser"] = ua_data["browser"]["name"]
			u["device"] = ua_data["device"]["name"]
			u["device-type"] = ua_data["device"]["type"]
		except:
			pass
	data = {"os": [], "client": [], "browser": [], "device": [], "device-type": []}
	for a in list(set([x["os"] for x in analytics if "os" in x])):
		data["os"].append(
			{
				"_id": a,
				"count": sum(
					[
						len(users["players"])
						for users in analytics
						if "os" in users
						if users["os"] == a
					]
				),
			}
		)
	for b in list(set([x["client"] for x in analytics if "client" in x])):
		data["client"].append(
			{
				"_id": b,
				"count": sum(
					[
						len(users["players"])
						for users in analytics
						if "client" in users
						if users["client"] == b
					]
				),
			}
		)
	for c in list(set([x["browser"] for x in analytics if "browser" in x])):
		data["browser"].append(
			{
				"_id": c,
				"count": sum(
					[
						len(users["players"])
						for users in analytics
						if "browser" in users
						if users["browser"] == c
					]
				),
			}
		)
	for d in list(set([x["device"] for x in analytics if "device" in x])):
		data["device"].append(
			{
				"_id": d,
				"count": sum(
					[
						len(users["players"])
						for users in analytics
						if "device" in users
						if users["device"] == d
					]
				),
			}
		)
	for e in list(set([x["device-type"] for x in analytics if "device-type" in x])):
		data["device-type"].append(
			{
				"_id": e,
				"count": sum(
					[
						len(users["players"])
						for users in analytics
						if "device-type" in users
						if users["device-type"] == e
					]
				),
			}
		)

	return jsonify(data)

@decentraland_api.route("/genesis-city/by-wallet/<event_type>", methods=["GET", "POST"])
@decentraland_api.route("/by-wallet/<event_type>", methods=["GET", "POST"])
@jwt_required()
def event_by_wallet(event_type):
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401

	# scene ping rate
	try:
		scene_parcels = [[str(x[0]) + "," + str(x[1])] for x in req["parcels"]]
		pollingInterval = list(
			db.analytics.find(
				{
					"eventName": "load-timer",
					"sceneName": req["scene"],
					"data.sceneInitData.parcels": scene_parcels,
				}
			)
			.sort("timestamp", -1)
			.limit(1)
		)[0]["data"]["sceneInitData"]["pollingInterval"]
		ping_multiplier = pollingInterval / 5000
	except:
		ping_multiplier = 1  # 5 second is default, so 1 * 5
	try:
		analytics = db.analytics.aggregate(
			filters.matchForMongo(req)
			+ [
				{"$match": {"eventName": event_type}},
				{"$group": {"_id": "$player", "count": {"$sum": 1 * ping_multiplier}}}
			]
		)
		analytics = list(analytics)
	except:
		return {"msg": "DB call failed"}, 500

	users = list(set([str(x["_id"]) for x in analytics]))

	# cumulative
	user_data = list(
		db.users.aggregate(
			[{"$match": {"address": {"$in": users}, "guest": {"$ne": None}}}]
		)
	)
	for a in analytics:
		try:
			user = [x for x in user_data if x["address"] == a["_id"]][0]
			a["address"] = a["_id"]
			# DCL Data
			a["dclName"] = user["avatar"]["avatar"]["name"]
			a["avatar"] = user["avatar"]["avatar"]["face"]
			#replace old urls without touching DB
			a["avatar"] = a["avatar"].replace("peer.kyllian.me","catalyst.atlascorp.io")
			a["avatar"] = a["avatar"].replace("peer-wc1.decentraland.org","catalyst.atlascorp.io")
			a["avatar"] = a["avatar"].replace("peer-eu1.decentraland.org","catalyst.atlascorp.io")
			a["avatar"] = a["avatar"].replace("peer-ec1.decentraland.org","catalyst.atlascorp.io")
			a["avatar"] = a["avatar"].replace("interconnected.online","catalyst.atlascorp.io")

			a["guest"] = user["guest"]
		except:
			a["address"] = a["_id"]

		try:
			user = {}
			user = [x for x in user_data if x["address"] == a["_id"]][0]
			a["guest"] = user["guest"]
		except:
			if "avatar" not in user:
				a["guest"] = True
			else:
				if "avatar" not in user["avatar"]:
					a["guest"] = True
				else:
					a["guest"] = False

		del a["_id"]

	return jsonify({"data": analytics})

######### EVENT QUERIES ###############
# for chart of events per day (line, bar) - [date, cumulative sum, daily sum]
@decentraland_api.route("/genesis-city/events-per-day/<event_type>", methods=["GET", "POST"])
@decentraland_api.route("/events-per-day/<event_type>", methods=["GET", "POST"])
@jwt_required()
def events_per_day(event_type):
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401
	start = dateutil.parser.isoparse(req["start"])
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "hour"
	if dt.total_seconds() >= 259200:
		unit = "day"
	try:
		analytics = list(
			db.analytics.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$match": {"eventName": event_type}},
					{
						"$group": {
							"_id": {
								"$dateTrunc": {
									"date": {"$toDate": "$timestamp"},
									"unit": unit,
									"timezone": filters.timezoneOffset(req),
									"binSize": 1,
								}
							},
							"count": {"$sum": 1},
						}
					},
					{"$sort": {"_id": 1}},
				]
			)
		)
		# cumulative
		analytics = filters.pad_zeroes(
			req, analytics, bin=3600 if unit == "hour" else 86400
		)
		events = []
		sum = 0
		for i in analytics:
			sum += i["count"]
			events.append({"_id": i["_id"], "count": i["count"], "cumulative": sum})
	except Exception as e:
		print(e)
		return {"msg": "DB call failed"}, 500
	return jsonify({"data": events})

@decentraland_api.route("/genesis-city/emote-trends", methods=["GET", "POST"])
@decentraland_api.route("/emoji-trends", methods=["GET", "POST"])
@decentraland_api.route("/emote-trends", methods=["GET", "POST"])
@jwt_required()
def emote_trends():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401
	start = dateutil.parser.isoparse(req["start"])
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "hour"
	if dt.total_seconds() >= 259200:
		unit = "day"
	try:
		analytics = list(
			db.analytics.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$match": {"eventName": "player-animation"}},
					{
						"$group": {
							"_id": {
								"emoji":"$data.expression",
								"date":{"$dateTrunc": {
									"date": {"$toDate": "$timestamp"},
									"unit": unit,
									"timezone": filters.timezoneOffset(req),
									"binSize": 1,
								}
							}},
							"count": {"$sum": 1},
						}
					},
					{
						"$group": {
							"_id": "$_id.date",
							"emojis": {
								"$push": {
									"k": "$_id.emoji",  
									"v": "$count"
								}
							}
						}
					},
					{
						"$addFields": {
							"emojis": {
								"$arrayToObject": "$emojis"
							}
						}
					},
					{
						"$replaceRoot": {
							"newRoot": {
								"$mergeObjects": [{ "_id": "$_id" }, "$emojis"]
							}
						}
					},				  
					{"$sort": {"_id": 1}},
				]
			)
		)

	except Exception as e:
		print(e)
		return {"msg": "DB call failed"}, 500

	emote_lookup = {}
	data = []
	for a in analytics:
		d = {}
		for key in a:
			if 'urn:decentraland:matic:collections-v2:' in key:
				# the contract returns a tuple, the 5th element contains meta data with the 3rd item being name
				if key not in emote_lookup:
					count = a[key]
					emote_name = emote_metadata(key)[5].split(":")[2]
					emote_lookup[key] = emote_name
					d[emote_name] = count
				else:
					count = a[key]
					d[emote_name] = count
			else:
				d[key] = a[key]
		data.append(d)

	data = filters.pad_zeroes(
		req, data, bin=3600 if unit == "hour" else 86400
	)

	return jsonify({"data": data})

@decentraland_api.route("/genesis-city/emotes", methods=["GET", "POST"])
@decentraland_api.route("/emojis", methods=["GET", "POST"])
@decentraland_api.route("/emotes", methods=["GET", "POST"])
@jwt_required()
def emotes():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401
	start = dateutil.parser.isoparse(req["start"])
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "hour"
	if dt.total_seconds() >= 259200:
		unit = "day"
	try:
		analytics = list(
			db.analytics.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$match": {"eventName": "player-animation"}},
					{
						"$group": {
							"_id": "$data.expression",
							"count": {"$sum": 1}
						}
					},
					{
						"$project": {
							"_id": 0,  
							"emote": "$_id",
							"count": "$count"
						}
					},				   
					{"$sort": {"count": -1}}, 
					{
						"$facet": {
							"topEmotes": [{"$limit": 8}],  
							"otherEmotes": [
								{"$skip": 8},  
								{
									"$group": {
										"_id": "Other",
										"count": {"$sum": "$count"}
									}
								},
									{
										"$project": {
											"_id": 0, 
											"emote": "$_id", 
											"count": 1  
										}
									}
							]
						}
					},
					{
						"$project": {
							"emotes": {"$setUnion": ["$topEmotes", "$otherEmotes"]}
						}
					},
					{"$unwind": "$emotes"},
					{"$replaceRoot": {"newRoot": "$emotes"}},
				]
			)
		)

	except Exception as e:
		print(e)
		return {"msg": "DB call failed"}, 500
	#emote resolution
	try:
		for a in analytics:
			if 'emote' in a and 'urn:decentraland:matic:collections-v2:' in a['emote']:
				# the contract returns a tuple, the 5th element contains meta data with the 3rd item being name
				a['emote'] = emote_metadata(a['emote'])[5].split(":")[2] 
	except:
		print('emote resolution failure',flush=True)


	return jsonify({"data": analytics})

######### MULTIPLE EVENTS QUERIES ###############
# for chart of events per day (line, bar) - [date, cumulative sum, daily sum]
@decentraland_api.route("/genesis-city/events-per-day", methods=["POST"])
@decentraland_api.route("/events-per-day", methods=["POST"])
@jwt_required()
def events_per_day_multiple():
	req = request.json
	if "events" not in req:
		return {"msg": "Invalid API Call"}, 500

	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401

	start = dateutil.parser.isoparse(req["start"])
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "hour"
	if dt.total_seconds() >= 259200:
		unit = "day"
	try:
		analytics = list(
			db.analytics.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$match": {"eventName": {"$in": req["events"]}}},
					{
						"$group": {
							"_id": {
								"date": {
									"$dateTrunc": {
										"date": {"$toDate": "$timestamp"},
										"unit": unit,
										"timezone": filters.timezoneOffset(req),
										"binSize": 1,
									}
								},
								"eventName": "$eventName",
							},
							"count": {"$sum": 1},
						}
					},
					{
						"$group": {
							"_id": "$_id.date",
							"events": {
								"$push": {
									"k": "$_id.eventName",
									"v": "$count",
								}
							},
							"totalCount": {"$sum": "$count"},
						}
					},
					{
						"$project": {
							"events": {"$arrayToObject": "$events"},
							"totalCount": 1,
						}
					},
					{"$replaceWith": {"$mergeObjects": ["$$ROOT", "$events"]}},
					{"$project": {"events": 0}},
					{"$sort": {"_id": 1}},
				]
			)
		)
		# cumulative
		analytics = filters.pad_zeroes_realms(
			req, analytics, bin=3600 if unit == "hour" else 86400
		)
		events = []
		sum = 0
		for i in analytics:
			if "totalCount" in i:
				sum += i["totalCount"]
			events.append({**i, "cumulative": sum})
	except Exception as e:
		print(e)
		return {"msg": "DB call failed"}, 500
	return jsonify({"data": events})

# events by unique user
@decentraland_api.route("/genesis-city/user-history/<user>/<page>", methods=["GET", "POST"])
@decentraland_api.route("/genesis-city/user-history/<user>", methods=["GET", "POST"])
@decentraland_api.route("/user-history/<user>", methods=["GET", "POST"])
@decentraland_api.route("/user-history/<user>/<page>", methods=["GET", "POST"])
@jwt_required()
def user_history(user,page=None):
	if page:
		skip = int(page) * 50
		pagination = [{"$skip": skip},{"$limit": 50}]
	else:
		pagination = []
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401
	#try:
	analytics = list(
		db.analytics.aggregate(
			filters.scene_filter(req)
			+ filters.time_bands(req)
			+ filters.parcel_filter(req)
			+ filters.branch_filter(req)
			+ filters.realm_filter(req)
			+ [
				{"$match": {"player": user.lower(),"eventName":{"$ne":"ping"}}},
				{"$project":{'player':1,'playerPosition':1,'parcel':1,'eventName':1,'timestamp':1,'data':{'$ifNull':['$data',None]},'sceneInitData': {"$ifNull": ["$sceneInitData", None]},'_id':0}},
				{"$sort": {"timestamp": 1}},			   
				{
					'$facet': {
						'totalCount': [{'$count': 'count'}],
						'data': pagination
					}
				},
				{
					'$addFields': {
						'totalCount': {
							'$arrayElemAt': ['$totalCount.count', 0]
						}
					}
				}]
		)
	)
	#
	#flesh out emote names and load time
	emote_lookup = {}
	analytics = analytics[0]
	for a in analytics['data']:
		if 'data' in a:
			if a['data']:
				if 'expression' in a['data']:
					emote = a['data']['expression']
					if 'urn:decentraland:matic:collections-v2:' in emote:
						# the contract returns a tuple, the 5th element contains meta data with the 3rd item being name
						if emote not in emote_lookup:
							a['data']['expression'] = emote_metadata(key)[5].split(":")[2]
						emote_lookup[emote] = emote_name
				if 'sceneInitData' in a['data']:
					if a['data']['sceneInitData']:
						a['loaddata'] = {
							"load-time": float(a['data']['sceneInitData']['endTime']) - float(a['data']['sceneInitData']['startTime']),
							"platform":a['data']['sceneInitData']['platform'],
							"parcels":a['data']['sceneInitData']['parcels'],
							"playerName":a['data']['playerName']
						}
					del a['data']

	#except:
	#	return {"msg": "DB call failed"}, 500
	return jsonify(analytics)

@decentraland_api.route("/genesis-city/summary/<event_type>", methods=["POST"])
@decentraland_api.route("/summary/<event_type>", methods=["POST"])
@jwt_required()
def event_summary(event_type):
	req = request.json

	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401

	# scene ping rate
	try:
		scene_parcels = [[str(x[0]) + "," + str(x[1])] for x in req["parcels"]]
		pollingInterval = list(
			db.analytics.find(
				{
					"eventName": "load-timer",
					"sceneName": req["scene"],
					"data.sceneInitData.parcels": scene_parcels,
				}
			)
			.sort("timestamp", -1)
			.limit(1)
		)[0]["data"]["sceneInitData"]["pollingInterval"]
		ping_multiplier = pollingInterval / 5000
	except Exception as e:
		print(e)
		ping_multiplier = 1  # 5 second is default, so 1 * 5
	try:
		analytics = db.analytics.aggregate(
			filters.matchForMongo(req)
			+ [
				{"$match": {"eventName": event_type}},
				{"$group": {"_id": None, "count": {"$sum": 1 * ping_multiplier}}},
			]
		)
		analytics = list(analytics)
	except:
		return {"msg": "DB call failed"}, 500

	count = 0
	if len(analytics):
		count = analytics[0]["count"]

	return jsonify({"data": {"count": count}})

@decentraland_api.route("/genesis-city/summary/<event_type>/average", methods=["POST"])
@decentraland_api.route("/summary/<event_type>/average", methods=["POST"])
@jwt_required()
def event_summary_average(event_type):
	req = request.json

	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401

	# scene ping rate

	try:
		scene_parcels = [[str(x[0]) + "," + str(x[1])] for x in req["parcels"]]
		pollingInterval = list(
			db.analytics.find(
				{
					"eventName": "load-timer",
					"sceneName": req["scene"],
					"data.sceneInitData.parcels": scene_parcels,
				}
			)
			.sort("timestamp", -1)
			.limit(1)
		)[0]["data"]["sceneInitData"]["pollingInterval"]
		ping_multiplier = pollingInterval / 5000
	except Exception as e:
		print(e)
		ping_multiplier = 1  # 5 second is default, so 1 * 5
	try:
		analytics = db.analytics.aggregate(
			filters.matchForMongo(req)
			+ [
				{"$match": {"eventName": event_type}},
				{"$group": {"_id": "$player", "count": {"$sum": 1 * ping_multiplier}}},
				{"$group": {"_id": None, "count": {"$avg": "$count"}}},
			]
		)
		analytics = list(analytics)
	except:
		return {"msg": "DB call failed"}, 500

	count = 0
	if len(analytics):
		count = analytics[0]["count"]

	return jsonify({"data": {"count": count}})


######### GEOGRAPHY QUERIES ###############
# unique wallets (ETH addresses) by geo
@decentraland_api.route("/wallets-by-geo", methods=["GET", "POST"])
@decentraland_api.route("/wallets-by-geo/<country>", methods=["GET", "POST"])
@decentraland_api.route(
	"/wallets-by-geo/<country>/<granularity>", methods=["GET", "POST"]
)
@jwt_required()
def wallets_by_geo(country="United States", granularity=False):
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401
	try:
		if country == "*":
			country = {"$ne" : None}
		analytics = list(
			db.analytics.aggregate(
				filters.matchForMongo(req)
				+ [
					{"$group": {"_id": "$player", "count": {"$sum": 1}}},
					{"$sort": {"_id": 1}},
				]
			)
		)
	except:
		return {"msg": "DB call failed"}, 500
	users = [x["_id"] for x in analytics if x["_id"]]
	if not granularity:
		event_users = db.users.aggregate(
			[
				{"$match": {"address": {"$in": users}}},
				{
					"$group": {
						"_id": None,
						"unique-wallets": {
							"$addToSet": {
								"address": "$address",
								"country": "$location.country",
							}
						},
					}
				},
				{"$unwind": "$unique-wallets"},
				{"$project": {"_id": 0}},
				{"$group": {"_id": "$unique-wallets.country", "count": {"$sum": 1}}},
			]
		)
		event_users = list(event_users)
	if granularity == "country":
		event_users = db.users.aggregate(
			[
				{"$match": {"address": {"$in": users}, "location.country": country}},
				{
					"$group": {
						"_id": None,
						"unique-wallets": {
							"$addToSet": {
								"address": "$address",
								"country": "$location.country",
							}
						},
					}
				},
				{"$unwind": "$unique-wallets"},
				{"$project": {"_id": 0}},
				{"$group": {"_id": "$unique-wallets.country", "count": {"$sum": 1}}},
			]
		)
		event_users = list(event_users)
	elif granularity == "region":
		event_users = db.users.aggregate(
			[
				{"$match": {"address": {"$in": users}, "location.country": country}},
				{
					"$group": {
						"_id": None,
						"unique-wallets": {
							"$addToSet": {
								"address": "$address",
								"country": "$location.country",
								"regionName": "$location.regionName",
							}
						},
					}
				},
				{"$unwind": "$unique-wallets"},
				{"$project": {"_id": 0}},
				{
					"$group": {
						"_id": "$unique-wallets.regionName",
						"country": {"$addToSet": "$unique-wallets.country"},
						"count": {"$sum": 1},
					}
				}
			]
		)
		event_users = list(event_users)
		response = {}
		for e in event_users:
			if e["country"][0] not in response:
				response[e["country"][0]] = {}
			response[e["country"][0]][e["_id"]] = e["count"]

			#e["country"] = e["country"][0]
		#event_users = sorted(event_users,key=lambda x: x["country"])

	elif granularity == "city":
		event_users = db.users.aggregate(
			[
				{"$match": {"address": {"$in": users}, "location.country": country}},
				{
					"$group": {
						"_id": None,
						"unique-wallets": {
							"$addToSet": {
								"address": "$address",
								"country": "$location.country",
								"region": "$location.regionName",
								"city": "$location.city",
							}
						},
					}
				},
				{"$unwind": "$unique-wallets"},
				{"$project": {"_id": 0}},
				{
					"$group": {
						"_id": "$unique-wallets.city",
						"country": {"$addToSet": "$unique-wallets.country"},
						"region": {"$addToSet": "$unique-wallets.region"},
						"count": {"$sum": 1},
					}
				}			  
			]
		)
		event_users = list(event_users)
		response = {}
		for e in event_users:
			if e["country"][0] not in response:
				response[e["country"][0]] = {}
			if e["region"][0] not in response[e["country"][0]]:
				response[e["country"][0]][e["region"][0]] = {}
			response[e["country"][0]][e["region"][0]][e["_id"]] = e["count"]

	if not event_users:
		return None
	else:
		return jsonify({'data': response})

@decentraland_api.route("/world-heat-map", methods=["GET", "POST"])
@decentraland_api.route("/genesis-city/world-heat-map", methods=["GET", "POST"])
@jwt_required()
def world_heat_map():
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401
	# try:
	event_users = list(
		db.analytics.aggregate(
			filters.matchForMongo(req)
			+ [
				{"$project": {"_id": 0, "player": 1, "auth": 1, "ip": 1}},
				{"$group": {"_id": "$ip"}},
				{
					"$lookup": {
						"from": "users",
						"localField": "_id",
						"foreignField": "ip",
						"as": "location",
					}
				},
				{"$project": {"location": {"$first": "$location.location.country"}}},
				{"$group": {"_id": "$location", "count": {"$sum": 1}}},
				{"$project": {"name": "$_id", "count": 1, "_id": 0}},
			]
		)
	)

	for u in event_users:
		if u["name"] is None:
			u["code"] = None
		else:
			u["code"] = constants.country_codes[u["name"]]

	maxCount = 0
	if len(event_users):
		maxCount = max([x["count"] for x in event_users])

	return jsonify({"data": event_users, "max": maxCount})


def a_parcel_from_scene(sceneName):
	parcel = (
		db.analytics.find({"sceneName": sceneName}).sort("timestamp", -1).limit(1)[0]
	)
	x = parcel["parcel"][0]
	y = parcel["parcel"][1]
	return x, y


@decentraland_api.route("/scene-map-from-name/<sceneName>")
def scene_map_from_name(sceneName, keep_in_house=False):
	x, y = a_parcel_from_scene(sceneName)
	parcels = map.scene_parcels(x, y, True)
	image_bytes = map.atlas_map(parcels["parcels"])
	return send_file(image_bytes, mimetype="image/png")

@decentraland_api.route("/parcels-captured-by-scene/<scene>")
@jwt_required()
def parcels_captured_by_scene(scene, keep_in_house=False):
	parcels = list(
		db.analytics.aggregate(
			[{"$match": {"sceneName": scene}}, {"$group": {"_id": "$parcel"}}]
		)
	)
	parcels = sorted([x["_id"] for x in parcels], key=lambda x: (x[0], x[1]))
	return jsonify({"data": parcels})

@decentraland_api.route("/atlas-coverage")
@decentraland_api.route("/atlas-coverage/<eventName>")
def atlas_coverage(eventName=None):
	if eventName:
		event_filter = [{"$match": {"eventName": eventName}}]
	else:
		event_filter = []
	parcels = list(
		db.analytics.aggregate(
			event_filter
			+ [{"$project": {"_id": 0, "parcel": 1}}, {"$group": {"_id": "$parcel"}}]
		)
	)
	parcels = [str(x["_id"][0]) + "," + str(x["_id"][1]) for x in parcels]
	image_bytes = map.atlas_map(parcels, 20)
	return send_file(image_bytes, mimetype="image/png")

@decentraland_api.route("/user-profile/<address>")
def user_profile(address, keep_in_house=False):
	profile = requests.get(
		"https://catalyst.atlascorp.io/lambdas/profiles/?id=" + str(address.lower())
	).json()
	if keep_in_house:
		return profile
	else:
		return jsonify(profile)

@decentraland_api.route("/redundant-users", methods=["GET", "POST"])
@jwt_required()
def redundant_users():
	if request.method == "POST":
		req = request.json
	else:
		req = None

	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401

	options = [
		"address",
		"ip",
		"userAgent",
		"realm",
		"guest",
	]  # ,"country","region","city"]
	if "filters" in req:
		choices = req["filters"]
	else:
		choices = []
	not_selected = list(set(options) - set(choices))
	group_function = {"$group": {"_id": {}}}
	counters = [{"$addFields": {"address-count": {"$size": "$player"}}}]

	for c in choices:
		# it's player not address in the db
		if c == "address":
			c = "player"
		if c in ["country", "region", "city"]:
			group_function["$group"]["_id"][c] = "$" + str(c)
		else:
			group_function["$group"]["_id"][c] = "$" + str(c)
	for n in not_selected:
		if n == "address":
			n = "player"
		else:
			counters.append(
				{"$addFields": {str(n) + "-count": {"$size": "$" + str(n)}}}
			)
		group_function["$group"][n] = {"$addToSet": "$" + str(n)}

	analytics = list(
		db.analytics.aggregate(
			filters.matchForMongo(req)  
			+ [group_function]
			+ counters
			+ [
				{
					"$project": {
						"ip": 0,
						"userAgent": 0,
						"realm": 0,
						"guest": 0,
						"country": 0,
						"region": 0,
						"city": 0,
					}
				}
			]
			+ [{"$sort": {"address-count": -1}}]
		)
	)

	# user information
	for a in analytics:
		if "ip" in a["_id"]:
			a["_id"]["ip"] = "..." + str(a["_id"]["ip"].split(".")[-1])
		if "userAgent" in a["_id"]:
			a["_id"]["userAgent"] = "..." + a["_id"]["userAgent"][-10:-1]
		culprits = []
		for p in a["player"]:
			try:
				username = list(db.users.find({"address": p}).limit(1))[0]
				culprits.append(
					{
						"address": p,
						"name": username["avatar"]["avatar"]["name"],
						"face": username["avatar"]["avatar"]["face"],
					}
				)
			except:
				culprits.append({"address": p, "name": "Guest", "face": ""})
			a["culprits"] = culprits
	return jsonify({"data": analytics})

@decentraland_api.route("/error-logs", methods=["POST"])
@jwt_required()
def getErrorLogs():
	req = request.json

	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401

	try:
		data = list(
			db.analytics.aggregate(
				filters.matchForMongo(req)
				+ [{"$match": {"eventName": "error-log"}}]
				+ [
					{
						"$addFields": {
							"error": "$data.logData.error",
							"level": "$data.logData.level",
						}
					}
				]
				+ [{"$project": {"ip": 0}}]
			)
		)
	except Exception as e:
		print(e)
		pass

	return json_util.dumps(data), 200

@decentraland_api.route("/error-logs/users", methods=["POST"])
@jwt_required()
def getErrorLogsUsers():
	req = request.json
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401
	match = filters.matchForMongo(req)[0]["$match"]

	match.pop("timestamp", None)

	data = db.analytics.find_one({**match, "eventName": "error-log"})

	usedErrorLogs = True if data else False
	return {"usedErrorLogs": usedErrorLogs}, 200


@decentraland_api.route("/retention/<retention_period>", methods=["GET", "POST"])
@jwt_required()
def retention(retention_period=7):
	if request.method == "POST":
		req = request.json
	else:
		req = None
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401

	date_range = [dateutil.parser.isoparse(req["start"]) + datetime.timedelta(days=x) for x in range(0, (dateutil.parser.isoparse(req["end"])-dateutil.parser.isoparse(req["start"])).days+1)]
	response = {"data":[]}
	for d in date_range:
		analytics = db.analytics.aggregate(
			[
				{
				"$match": { 
					'sceneName': req['scene'], 
					'parcel': {'$in': req['parcels']}, 
					'timestamp': {
						'$lt': d.timestamp() * 1000, 
						'$gte': (d - datetime.timedelta(days=1)).timestamp() * 1000
						}
					}
				},
				{"$addFields":{"date": {"$dateTrunc": {"date": { "$toDate": "$timestamp" },"unit": "day"}}}},
				{"$group":{"_id": {"player": "$player","date": "$date"}}},

				{"$lookup":{
					"from": "analytics", 
					"let": { "player_id": "$_id.player", "current_date": "$_id.date" },
					"pipeline": [
						{
							"$match": { 
								'sceneName': req['scene'], 
								'parcel': {'$in': req['parcels']}, 
								'timestamp': {
									'$lt': (d - datetime.timedelta(days=1)).timestamp() * 1000, 
									'$gte': (d - datetime.timedelta(days=int(retention_period) + 1)).timestamp() * 1000
									}
								
							}
						},
					{
						"$match": {
						"$expr": {
							"$and": [
							{ "$eq": ["$player", "$$player_id"] },
							{ 
								"$gte": [
								{ "$dateTrunc": { "date": { "$toDate": "$timestamp" }, "unit": "day" } }, 
								{ "$dateSubtract": { "startDate": "$$current_date", "unit": "day", "amount": int(retention_period) } }
								] 
							},
							{ 
								"$lt": [
								{ "$dateTrunc": { "date": { "$toDate": "$timestamp" }, "unit": "day" } }, 
								"$$current_date"
								] 
							}
							]
						} 
						}
					},
					{"$project": { "_id": 0,"player":1,"timestamp":1} } 
					],
					"as": "previous_activities"
				}},
				{"$project":
					{
						"player": "$_id.player",
						"date": "$_id.date",
						"retained": {
						"$cond": { "if": { "$gt": [{ "$size": "$previous_activities" }, 0] }, "then": True, "else": False }
						}
					}}
			]
		)
		analytics = list(analytics)
		print(analytics,flush=True)
		data = {"date":d,"retention":len([x for x in analytics if x["retained"]]),"players":[x['player'] for x in analytics if x['retained']]}
		if data["retention"] > 0:
			data["retention_%"] =  data["retention"]/len(analytics)
		else:	
			data["retention_%"] = 0
		response['data'].append(data)

	return jsonify(response)

@decentraland_api.route("/genesis-city/stickiness", methods=["POST"])
@decentraland_api.route("/stickiness", methods=["POST"])
@jwt_required()
def stickiness():
	req = request.json

	### Scene Acess control ###
	if not has_access(request.headers["Authorization"].split(" ")[1], req):
		return {"msg": "User does not have access to scene"}, 401

	if not (req["start"] and req["end"]):
		return {"msg": "invalid request"}, 500

	start = dateutil.parser.isoparse(req["start"]) # Extend back one month to get MAUs
	end = dateutil.parser.isoparse(req["end"])
	dt = end - start
	unit = "day"
	analytics = []

	#Rolling MAUs
	current_date = start
	while current_date <= end:
		pipeline = filters.matchForMongo(req,False) +[
			
			{
				"$match": {
					"timestamp": {"$gte": (current_date - datetime.timedelta(days=30)).timestamp() * 1000, "$lt": (current_date + datetime.timedelta(days=1)).timestamp() * 1000}
				}
			},
			{
				"$group": {
					"_id": None,
					"uniqueUsers": {"$addToSet": "$player"}
				}
			},
			{
				"$project": {
					"date": current_date,
					"MAU": {"$size": "$uniqueUsers"}
				}
			}
		]

		# Calculate MAUs
		maus = list(db.analytics.aggregate(pipeline))
		mau_count = maus[0]['MAU'] if maus else 0

		# Calculate DAUs
		dau_pipeline = filters.matchForMongo(req,True) + [
			{
				"$match": {
					"timestamp": {
						"$gte": current_date.timestamp() * 1000,
						"$lt": (current_date + datetime.timedelta(days=1)).timestamp() * 1000
					}
				}
			},
			{
				"$group": {
					"_id": None,
					"uniqueUsers": {"$addToSet": "$player"}
				}
			},
			{
				"$project": {
					"DAU": {"$size": "$uniqueUsers"}
				}
			}
		]
		daus = list(db.analytics.aggregate(dau_pipeline))
		dau_count = daus[0]['DAU'] if daus else 0

		# Calculate Stickiness
		stickiness = dau_count / mau_count if mau_count > 0 else 0
		analytics.append({"date": current_date.strftime("%Y-%m-%d"), "DAU": dau_count, "MAU": mau_count, "stickiness": stickiness})
		current_date += datetime.timedelta(days=1)
	return jsonify({"data": analytics})

### GOOFY STUFF ###
@decentraland_api.route("/phasers", methods=["POST"])
def phasers(req=None, keep_in_house=False):
	if request.method == "POST":
		req = request.json
	else:
		req = None

	# lock on the front door
	if "password" not in req or req["password"] != "timecops":
		return {"msg": "User not part of the Time Cops Division"}, 401

	analytics = list(
		db.analytics.aggregate(
			filters.scene_filter(req)
			+ filters.time_bands(req)
			+ filters.parcel_filter(req)
			+ filters.branch_filter(req)
			+ filters.realm_filter(req)
			+ [
				{"$project": {"date": 1, "realm": 1, "player": 1}},
				{
					"$group": {
						"_id": {
							"date": {
								"$dateTrunc": {
									"date": {"$toDate": "$timestamp"},
									"unit": "second",
									"timezone": filters.timezoneOffset(req),
									"binSize": filters.bin_size(req),
								}
							},
							"realm": "$realm",
						},
						"wallets": {"$addToSet": "$player"},
					}
				},
				{
					"$group": {
						"_id": "$_id.date",
						"realms": {"$push": {"k": "$_id.realm", "v": "$wallets"}},
					}
				},
				{"$project": {"realmData": {"$arrayToObject": "$realms"}}},
				{"$replaceWith": {"$mergeObjects": ["$$ROOT", "$realmData"]}},
				{"$project": {"realmData": 0}},
				{"$sort": {"_id": 1}},
			]
		)
	)
	# get phaser footprints (across realms)
	phaser_data = []
	culprits = []
	for a in analytics:
		realms = [key for key in a if key != "_id"]
		users = [item for sublist in [a[r] for r in realms] for item in sublist]
		user_counts = Counter(users)
		phasers = {
			key: val
			for key, val in user_counts.items()
			if (isinstance(val, int) and (val > 1))
		}
		for phaser in phasers:
			culprits.append(phaser)
			phasers[phaser] = []
			for r in realms:
				if phaser in a[r]:
					phasers[phaser].append(r)
		phaser_data.append({"_id": a["_id"], "phasers": phasers})

	# create phaser reports (across time)
	phaser_reports = []
	citations = []
	culprits = list(set(culprits))
	for c in culprits:
		path = []
		last_list = []
		for v in phaser_data:
			if c in v["phasers"] and v["phasers"][c] != last_list:
				path.append({"timestamp": v["_id"], "realms": v["phasers"][c]})
				last_list = v["phasers"][c]
			elif last_list != []:
				path.append({"timestamp": v["_id"], "realms": ["OFF WORLD"]})
				last_list = []

		# make sankey
		sankey = {"links": [], "nodes": []}
		first = True
		for p in range(0, len(path)):
			for r in path[p]["realms"]:
				sankey["nodes"].append(
					{
						"name": r + " " + str(path[p]["timestamp"]),
						"node": r + " " + str(path[p]["timestamp"]),
					}
				)
				if not first:
					for q in path[p - 1]["realms"]:
						sankey["links"].append(
							{
								"source": q + " " + str(path[p - 1]["timestamp"]),
								"target": r + " " + str(path[p]["timestamp"]),
								"value": 1 / len(path[p]["realms"]),
							}
						)
			first = False

		citation = {
			"culprit": c,
			"path": path,
			"sankey": sankey,
			"degree": max([len(x["realms"]) for x in path]),
		}
		citationNum = db.phasers.insert_one(citation)
		citation["_id"] = json.loads(json_util.dumps(citationNum.inserted_id))["$oid"]
		citations.append(citation["_id"])
		phaser_reports.append(citation)

	return jsonify(
		{"phasers": phaser_data, "reports": phaser_reports, "citations": citations}
	)

@decentraland_api.route("/phasers/<citation>", methods=["GET", "POST"])
def phaser_citation(citation, keep_in_house=False):
	if request.method == "POST":
		req = request.json
	else:
		req = None
	ticket = list(db.phasers.find({"_id": ObjectId(citation)}))
	if len(ticket) > 0:
		ticket = ticket[0]
		del ticket["_id"]
	else:
		ticket = ""
	return jsonify({"citation": ticket})


