from itertools import count
import re
from flask import Flask, jsonify, render_template, json, Response, request, send_file, Blueprint
from flask_cors import CORS, cross_origin
import requests
import os
import pymongo
import time
import pytz
import csv
import datetime
import dateutil.parser
import math
from dotenv import load_dotenv
from collections import Counter
from database import client, db, dao_client, dao_db
import filters

outside_in_api = Blueprint('outside_in', __name__)

@outside_in_api.route('/outside-in/users',methods=['GET','POST'])
def users():
    if request.method == "POST":
        req = request.json
    else:
        req = None

    analytics = list(dao_db.islands.aggregate(filters.time_bands(req) +  [
        {"$unwind":"$islands"},
        {"$unwind":"$islands.peers"}] + filters.parcel_filter(req,True) +[
        { "$group": {
                "_id": { "$dateTrunc": { "date": {"$toDate":"$timestamp"}, "unit": "second", "binSize": filters.bin_size(req) } }, 
                "unique-users": { "$addToSet": "$islands.peers.address" },
            }},
        { "$addFields": { "count": {"$size":"$unique-users"}}},
        { "$sort": { "_id": 1 } } 
        
    ]+ filters.include_users(req)))

    return jsonify({'analytics':analytics}),200

@outside_in_api.route('/outside-in/time-on-site',methods=['GET','POST'])
def time_on_site():
    if request.method == "POST":
        req = request.json
    else:
        req = None

    analytics = list(dao_db.islands.aggregate(filters.time_bands(req) +  [
        {"$unwind":"$islands"},
        {"$unwind":"$islands.peers"}] + filters.parcel_filter(req,True) +[
        {"$project":{"timestamp":1,".islands.peers.address":1}},
        { "$group": {
                "_id": { 
                    "date":{"$dateTrunc": { "date": {"$toDate":"$timestamp"}, "unit": "second", "binSize": filters.bin_size(req) }},
                    "user":"$islands.peers.address"
                    }, 
                "time-on-site": { "$sum": 20},
            }},
        { "$sort": { "_id": 1 } } 
        
    ]+ filters.include_users(req)))

    analytics = [{"date":a['_id']['date'],"user":a['_id']['user'],"time-on-site":a['time-on-site']} for a in analytics]
    with open("timeonsite.csv", "w") as f:

        dict_writer = csv.DictWriter(f, ["date","user","time-on-site"])
        dict_writer.writeheader()
        dict_writer.writerows(analytics)

    return send_file('./timeonsite.csv',mimetype="text/csv")

@outside_in_api.route('/outside-in/user-hangouts/<address>',methods=['GET','POST'])
def user_hangouts(address):
    if request.method == "POST":
        req = request.json
    else:
        req = None

    analytics = list(dao_db.islands.aggregate(filters.time_bands(req) +  [
        {"$match":{"islands.peers.address":{"$in":[address,address.lower()]}}},
        {"$unwind":"$islands"},
        {"$unwind":"$islands.peers"},
        { "$group": {
                "_id": "$islands.peers.parcel", 
                "time-on-site": { "$sum": 20 },
            }},
        { "$sort": { "time-on-site": -1 } } 
        
    ]))

    scenes = []
    return jsonify({'parcels':analytics,'scenes':scenes})
