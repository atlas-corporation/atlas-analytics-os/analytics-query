import os

import requests
from flask import Blueprint, jsonify

from database import db

# Global params
# we pay for user stack to obtain this information
userstack_api = os.environ.get("userstack_api")
user_agent_api = Blueprint("user_agents", __name__)

######## Update User Agent DB ########
@user_agent_api.route("/user-agents/update", methods=["GET"])
def user_agent_update(keep_in_house=False, create=False):
    print("Getting user agents from analytics db...", flush=True)
    user_agents_found = list(db.analytics.distinct("userAgent"))
    print("Getting currently identified user agents...", flush=True)
    user_agent_db = list(db.userAgents.distinct("_id"))
    for ua in user_agents_found:
        if ua not in user_agent_db:
            userstack = requests.get(
                "http://api.userstack.com/detect?access_key="
                + str(userstack_api)
                + "&ua="
                + str(ua)
            ).json()
            userstack["_id"] = userstack["ua"]
            db.userAgents.insert_one(userstack)
            print("Added " + str(ua), flush=True)

    if keep_in_house:
        return "All done processing " + str(len(user_agents_found)) + " user agents."
    else:
        return jsonify(
            "All done processing " + str(len(user_agents_found)) + " user agents."
        )
