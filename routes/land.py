import requests
from flask import Blueprint, jsonify
import os
from dotenv import load_dotenv

load_dotenv(os.path.join(os.path.dirname(__file__), ".env"))

the_graph_api = os.environ.get("THE_GRAPH_API")

land_api = Blueprint("land", __name__)

@land_api.route("/owned-land/<address>", methods=["GET"])
def owned_land(address, keep_in_house=False):
    graphQL_query = (
        '''
    {
    estates(first: 1000,where:{owner:"'''
        + address.lower()
        + '''"}) {
        data{
            name
        }
        parcels{
            x
            y
        }
    }
    parcels(first: 1000,where:{owner:"'''
        + address.lower()
        + """"}){
        x
        y
    }
    }
    """
    )
    url = "https://gateway.thegraph.com/api/" +str(the_graph_api) + "/subgraphs/id/CvkVuzibR3wKDcbeKVAMqa8qqnjLGpTfWbvWg4FaAzYT"
    res = requests.post(url, json={"query": graphQL_query})
    res = res.json()
    print(res,flush=True)
    land = {
        "parcels": [[int(x["x"]), int(x["y"])] for x in res["data"]["parcels"]],
        "estates": [
            {
                "name": x["data"]["name"],
                "parcels": [[int(y["x"]), int(y["y"])] for y in x["parcels"]],
            }
            for x in res["data"]["estates"]
        ],
    }
    if keep_in_house:
        return land
    else:
        return jsonify(land)
