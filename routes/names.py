import requests
from flask import Blueprint, jsonify
import os
from dotenv import load_dotenv

load_dotenv(os.path.join(os.path.dirname(__file__), ".env"))

the_graph_api = os.environ.get("THE_GRAPH_API")

names_api = Blueprint("names", __name__)

@names_api.route("/owned-names/<address>", methods=["GET"])
def owned_names(address, keep_in_house=False):
	graphQL_query = (
		'''
		{
			domains(first: 1000,where:{parent:"0x0058500cd570bacbce7dbb11497c280dea3f623a7479be7d8b01c2ae07b623fc",owner:"''' + str(address.lower()) + '''"}) {
				id
				name
				labelName
			}
		}
		'''
	)
	url = "https://gateway.thegraph.com/api/"+str(the_graph_api)+"/subgraphs/id/5XqPmWe6gjyrJtFn9cLy237i4cWw2j9HcUJEXsP5qGtH"
	res = requests.post(url, json={"query": graphQL_query})
	res = res.json()
	
	if len(res["data"]["domains"]) > 0:
		names = {"names": [x['name'] for x in res["data"]["domains"]]}
	else:
		names = {"names":[]}

	if keep_in_house:
		return names
	else:
		return jsonify(names)
