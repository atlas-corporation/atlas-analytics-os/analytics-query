import time

import requests
from flask import Blueprint, jsonify, request
from flask_jwt_extended import get_jwt_identity, jwt_required

import filters
from authentication import has_god_mode
from database import db

god_mode_api = Blueprint("god-mode", __name__)

#### GOD MODE ####
@god_mode_api.route("/god-mode/scene-access", methods=["GET", "POST"])
@jwt_required()
def god_mode_scene_access():
    if not has_god_mode(get_jwt_identity().get("address")):
        return {"msg": "User does not have access to data"}, 401

    if request.method == "POST":
        req = request.json
    else:
        req = None
    try:
        analytics = list(
            db.analytics.aggregate(
                filters.time_bands(req)
                + [
                    {
                        "$match": {
                            "eventName": "load-timer",
                            "data.sceneInitData.analyticsVersion": {
                                "$in": [
                                    "0.3",
                                    "0.4",
                                    "0.4.2",
                                    "0.4.3",
                                    "0.4.4",
                                    "0.4.5",
                                    "0.4.6",
                                    "0.4.7",
                                ]
                            },
                        }
                    },
                    {"$sort": {"timestamp": 1, "sceneName": 1}},
                    {
                        "$group": {
                            "_id": {
                                "sceneName": "$sceneName",
                                "parcels": "$data.sceneInitData.parcels",
                            },
                            "lastUpdate": {"$last": "$timestamp"},
                            "tags": {"$last": "$data.sceneInitData.tags"},
                        }
                    },
                ]
            )
        )
    except:
        return {"msg": "DB call failed"}, 500

    # Now get owners
    owner_lookup = [p["_id"]["parcels"][0] for p in analytics if "parcels" in p["_id"]]

    graphQL_query = (
        """
    {
        nfts(first:1000,where:{ searchText_in: """
        + str(owner_lookup).replace("'", '"')
        + """}) {
            parcel{
                x
                y
                owner{
                    id
                }
                estate{
                    id
                    tokenId
                    owner{
                        id
                    }
                }
            }
        }
    }"""
    )
    url = "https://api.thegraph.com/subgraphs/name/decentraland/marketplace"
    res = requests.post(url, json={"query": graphQL_query})
    res = res.json()
    for a in analytics:
        a["access"] = []
        if a["tags"]:
            a["access"] = [
                z.split("atlas:")[-1].lower() for z in a["tags"] if "atlas" in z
            ]
        if "parcels" in a:
            x, y = a["_id"]["parcels"][0].split(",")
            graph_data = [
                g
                for g in res["data"]["nfts"]
                if g["parcel"]["x"] == x
                if g["parcel"]["y"] == y
            ]
            if len(graph_data) > 0:
                graph_data = graph_data[0]
                print(a["access"], flush=True)
                print(graph_data, flush=True)
                try:
                    a["access"].append(graph_data["parcel"]["estate"]["owner"]["id"])
                except:
                    a["access"].append(graph_data["parcel"]["owner"]["id"])

    return jsonify(analytics)

# scene-records
@god_mode_api.route("/god-mode/scene-records", methods=["GET", "POST"])
@jwt_required()
def god_mode_scene_records():
    if not has_god_mode(get_jwt_identity().get("address")):
        return {"msg": "User does not have access to data"}, 401

    if request.method == "POST":
        req = request.json
    else:
        req = None
    try:
        analytics = list(
            db.analytics.aggregate(
                [
                    {
                        "$group": {
                            "_id": {"sceneName": "$sceneName"},
                            "count": {"$sum": 1},
                        }
                    },
                    {"$sort": {"count": -1}},
                ],
                allowDiskUse=True,
            )
        )
    except:
        return {"msg": "DB call failed"}, 500
    if req and "cache" in req and req["cache"]:
        db.godModeCache.insert_one(
            {
                "timestamp": req["timestamp"],
                "data-type": "scene-records",
                "analytics": analytics,
            }
        )
        return ""
    else:
        return jsonify(analytics)


@god_mode_api.route("/god-mode/scene-init", methods=["GET", "POST"])
@jwt_required()
def god_mode_scene_init():
    if not has_god_mode(get_jwt_identity().get("address")):
        return {"msg": "User does not have access to data"}, 401
    # Get Tags and other display data
    if request.method == "POST":
        req = request.json
    else:
        req = None
    try:
        analytics = list(
            db.analytics.aggregate(
                filters.time_bands(req)
                + [
                    {
                        "$match": {
                            "eventName": "load-timer",
                            "data.sceneInitData.analyticsVersion": {
                                "$in": [
                                    "0.3",
                                    "0.4",
                                    "0.4.2",
                                    "0.4.3",
                                    "0.4.4",
                                    "0.4.5",
                                    "0.4.6",
                                    "0.4.7",
                                ]
                            },
                        }
                    },
                    {
                        "$group": {
                            "_id": {
                                "sceneName": "$sceneName",
                                "parcels": "$data.sceneInitData.parcels",
                            },
                            "firstUpdate": {"$min": "$timestamp"},
                        }
                    },
                ],
                allowDiskUse=True,
            )
        )
    except:
        return {"msg": "DB call failed"}, 500
    if req and "cache" in req and req["cache"]:
        db.godModeCache.insert_one(
            {
                "timestamp": req["timestamp"],
                "data-type": "scene-init",
                "analytics": analytics,
            }
        )
    return jsonify(analytics)


@god_mode_api.route("/god-mode-cache/<data_type>", methods=["GET", "POST"])
@jwt_required()
def god_mode_cache(data_type=None):
    if not has_god_mode(get_jwt_identity().get("address")):
        return {"msg": "User does not have access to data"}, 401
    data = list(
        db.godModeCache.find({"data-type": data_type}, {"_id": 0})
        .sort("timestamp", -1)
        .limit(1)
    )[0]
    return jsonify(data)


@god_mode_api.route("/god-mode/num-scenes", methods=["GET", "POST"])
@jwt_required()
def god_mode_num_scenes():
    if not has_god_mode(get_jwt_identity().get("address")):
        return {"msg": "User does not have access to data"}, 401
    # Get Tags and other display data
    try:
        analytics = list(db.analytics.distinct("sceneName"))
    except:
        return {"msg": "DB call failed"}, 500

    return jsonify({"data": len(analytics)})


@god_mode_api.route("/god-mode/num-records", methods=["GET", "POST"])
@jwt_required()
def god_mode_num_records():
    if not has_god_mode(get_jwt_identity().get("address")):
        return {"msg": "User does not have access to data"}, 401
    try:
        analytics = db.analytics.count()
    except:
        return {"msg": "DB call failed"}, 500

    return jsonify({"data": analytics})


@god_mode_api.route("/god-mode/parcel-coverage", methods=["GET", "POST"])
@god_mode_api.route("/god-mode/parcel-coverage/<full>", methods=["GET", "POST"])
@jwt_required()
def god_mode_parcel_coverage(full=False):
    if not has_god_mode(get_jwt_identity().get("address")):
        return {"msg": "User does not have access to data"}, 401
    # Get Tags and other display data
    if request.method == "POST":
        req = request.json
    else:
        req = None
    # try:
    analytics = list(
        db.analytics.aggregate(
            [
                {"$project": {"parcel": 1}},
                {
                    "$group": {
                        "_id": "$parcel",
                    }
                },
            ],allowDiskUse=True,
        )
    )
    # except:
    #    return {"msg":"DB call failed"},500
    if not full:
        if req and "cache" in req and req["cache"]:
            db.godModeCache.insert_one(
                {
                    "timestamp": req["timestamp"],
                    "data-type": "parcel-coverage",
                    "analytics": {
                        "num-parcels": len(analytics),
                        "dcl-coverage": len(analytics) / 90601,
                        "total-parcels": 90601,
                    },
                }
            )
        

        return jsonify(
            {
                "data": {
                    "num-parcels": len(analytics),
                    # "parcels-covered":analytics,
                    "dcl-coverage": len(analytics) / 90601,
                    "total-parcels": 90601,
                }
            }
        )
    else:
        return jsonify(analytics)


@god_mode_api.route("/god-mode/num-users", methods=["GET", "POST"])
@jwt_required()
def god_mode_num_users():
    if not has_god_mode(get_jwt_identity().get("address")):
        return {"msg": "User does not have access to data"}, 401
    # Get Tags and other display data
    try:
        analytics = db.users.distinct("address")
    except:
        return {"msg": "DB call failed"}, 500

    return jsonify({"data": len(analytics)})


@god_mode_api.route("/god-mode/recent-app-users", methods=["GET", "POST"])
@jwt_required()
def god_mode_recent_app_users():
    if not has_god_mode(get_jwt_identity().get("address")):
        return {"msg": "User does not have access to data"}, 401
    now = (time.time() * 1000) - 86400000
    analytics = list(
        db.nonces.find({"nonce": {"$gte": now}}, {"_id": 0, "nonce": 1, "address": 1})
    )

    return jsonify({"data": {"users": analytics, "count": len(analytics)}})


@god_mode_api.route("/god-mode/daily-active-logins", methods=["GET", "POST"])
@jwt_required()
def god_mode_app_dals():
    if not has_god_mode(get_jwt_identity().get("address")):
        return {"msg": "User does not have access to data"}, 401

    try:
        analytics = list(
            db.nonces.aggregate(
                [
                    {
                        "$group": {
                            "_id": {
                                "$dateTrunc": {
                                    "date": {"$toDate": "$nonce"},
                                    "unit": "millisecond",
                                    "binSize": 86400000,
                                }
                            },
                            "count": {"$sum": 1},
                        }
                    },
                    {"$sort": {"_id": -1}},
                ]
            )
        )
    except:
        return {"msg": "DB call failed"}, 500
    return jsonify({"data": analytics})


@god_mode_api.route("/god-mode/app-daus", methods=["GET", "POST"])
@jwt_required()
def god_mode_app_daus():
    if not has_god_mode(get_jwt_identity().get("address")):
        return {"msg": "User does not have access to data"}, 401
    if request.method == "POST":
        req = request.json
    else:
        req = None
    try:
        analytics = list(
            db.nonces.aggregate(
                [
                    {
                        "$group": {
                            "_id": {
                                "$dateTrunc": {
                                    "date": {"$toDate": "$nonce"},
                                    "unit": "millisecond",
                                    "binSize": 86400000,
                                }
                            },
                            "unique-users": {"$addToSet": "$address"},
                        }
                    },
                    {"$addFields": {"count": {"$size": "$unique-users"}}},
                    {"$sort": {"_id": -1}},
                ]
                + filters.include_users(req)
            )
        )

    except:
        return {"msg": "DB call failed"}, 500

    if "include-users" in req and req["include-users"]:
        unique_users = {}
        for a in analytics:
            unique_names = []
            for u in a["unique-users"]:
                if u not in unique_users:
                    profile = requests.get(
                        "https://catalyst.atlascorp.io/lambdas/profiles/?id=" + str(u)
                    ).json()
                    try:
                        name = profile[0]["avatars"][0]["name"]
                        unique_users[u] = {"address": u, "name": name}
                    except:
                        unique_users[u] = {"address": u, "name": "???"}

                    try:
                        profilepic = profile[0]["avatars"][0]["avatar"]["snapshots"][
                            "face256"
                        ]
                        unique_users[u]["face"] = profilepic
                    except:
                        unique_users[u]["face"] = None
                    unique_names.append(unique_users[u])
                else:
                    unique_names.append(unique_users[u])

            a["unique-names"] = unique_names
            del a["unique-users"]

    if req and "cache" in req and req["cache"]:
        db.godModeCache.insert_one(
            {
                "timestamp": req["timestamp"],
                "data-type": "app-daus",
                "analytics": analytics,
            }
        )

    return jsonify({"data": analytics, "unique-users": unique_users})
