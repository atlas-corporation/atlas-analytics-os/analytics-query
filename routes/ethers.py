import asyncio

from flask import Blueprint, jsonify, request
from flask_jwt_extended import get_jwt_identity, jwt_required
from web3 import Web3

import ethers

ethers_api = Blueprint("ethers", __name__)

@ethers_api.route("/user/getTokenBalances")
@jwt_required()
def getUserTokenBalances():
    auth_header = request.headers.get("Authorization")
    if not auth_header:
        return jsonify({"error": "Missing Authorization header"}), 401

    address = get_jwt_identity().get("address")

    if not address:
        return 400, "unauthorized"

    balances = {
        **{
            key: value.functions["balanceOf"](address).call()
            for (key, value) in ethers.contracts.items()
        },
        "ETH": ethers.w3.eth.get_balance(address),
    }

    return balances, 200

@ethers_api.route("/user/getTokenBalances/<address>")
@jwt_required()
def getUserTokenBalancesAddress(address):
    if not address:
        return 400, "unauthorized"
    address = Web3.toChecksumAddress(address)
    balances = {
        **{
            key: value.functions["balanceOf"](address).call()
            for (key, value) in ethers.contracts.items()
        },
        "ETH": ethers.w3.eth.get_balance(address),
    }

    return balances, 200

@ethers_api.route("/user/getNFTBalances")
@jwt_required()
def getUserNFTBalances():
    auth_header = request.headers.get("Authorization")
    if not auth_header:
        return jsonify({"error": "Missing Authorization header"}), 401

    address = get_jwt_identity().get("address")

    if not address:
        return 400, "unauthorized"

    balances = {
        **{
            key: {"balance": value.functions["balanceOf"](address).call()}
            for (key, value) in ethers.NFTcontracts.items()
        },
    }

    data = asyncio.run(ethers.fetch_floor_data())
    data = {
        collection: data[i] for (i, collection) in enumerate(ethers.collection_names)
    }

    for key in balances.keys():
        balances[key]["floorPrice"] = data[key]

    return balances, 200

@ethers_api.route("/user/getNFTBalances/<address>")
@jwt_required()
def getUserNFTBalancesUser(address):
    if not address:
        return 400, "unauthorized"

    balances = {
        **{
            key: {"balance": value.functions["balanceOf"](address).call()}
            for (key, value) in ethers.NFTcontracts.items()
        },
    }

    data = asyncio.run(ethers.fetch_floor_data())
    data = {
        collection: data[i] for (i, collection) in enumerate(ethers.collection_names)
    }

    for key in balances.keys():
        balances[key]["floorPrice"] = data[key]

    return balances, 200

@ethers_api.route("/user/getENSName")
@jwt_required()
def getUserENSName():
    auth_header = request.headers.get("Authorization")
    if not auth_header:
        return jsonify({"error": "Missing Authorization header"}), 401

    address = get_jwt_identity().get("address")

    if not address:
        return 400, "unauthorized"

    name = ethers.ns.name(address)

    return name if name else address, 200

@ethers_api.route("/user/getENSName/<address>")
@jwt_required()
def getUserENSNameAddress(address):
    if not address:
        return 400, "unauthorized"

    name = ethers.ns.name(address)

    return name if name else address, 200
