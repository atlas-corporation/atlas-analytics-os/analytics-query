import datetime
import time

import pytz
import requests
from bson import ObjectId, json_util
from flask import Flask, json, jsonify, request
from flask_cors import CORS
from flask_jwt_extended import (
    JWTManager,
    create_access_token,
    get_jwt_identity,
    jwt_required,
)
from werkzeug.routing import FloatConverter as BaseFloatConverter

import constants
import routes
from authentication import has_god_mode
from database import client, db

class FloatConverter(BaseFloatConverter):
    regex = r"-?\d+(\.\d+)?"

utc = pytz.UTC

god_mode = [x.lower() for x in constants.god_mode]

print("Running analytics query server powered by Atlas Corp")
app = Flask(__name__)
CORS(app)
# before routes are registered
app.url_map.converters["float"] = FloatConverter

# register blueprints
app.register_blueprint(routes.outside_in.outside_in_api)
app.register_blueprint(routes.map.maps_api)
app.register_blueprint(routes.dynamic_maps.dynamic_maps_api)
app.register_blueprint(routes.land.land_api)
app.register_blueprint(routes.names.names_api)
app.register_blueprint(routes.userAgent.user_agent_api)
app.register_blueprint(routes.decentraland.decentraland_api)
app.register_blueprint(routes.hyperfy.hyperfy_api)
app.register_blueprint(routes.ethers.ethers_api)
app.register_blueprint(routes.godMode.god_mode_api)
app.register_blueprint(routes.worlds.worlds_api)

# Global params
# load_dotenv(os.path.join(os.path.dirname(__file__), ".env"))

# Setup the Flask-JWT-Extended extension
app.config["JWT_SECRET_KEY"] = ""  # Change this!
jwt = JWTManager(app)

port = 4200
host = "0.0.0.0"

@app.route("/", methods=["GET", "POST"])
def health():
    return jsonify("Analytics query back-end is up and running!")


@app.route("/scene-access/<address>", methods=["GET", "POST"])
def scene_access(address, keep_in_house=False):
    stopwatch = time.time()

    # genesis city scenes
    latest_scene_list = list(db["login-cache"].find())    
    # genesis city parcels owned
    owned_land = routes.land.owned_land(address, True)
    # worlds scenes
    worlds_scene_list = routes.worlds.worlds_scenes(True)
    # worlds ownership
    names = routes.names.owned_names(address,True)
    print(names,flush=True)
    
    # its good to be the king
    if address.lower() in god_mode:
        is_god_mode_user = True
        permissioned_land = [
            {
                "sceneName": x["sceneName"],
                "permissioned": True,
                "owned": False,
                "lastUpdate": x["updated"],
                "sceneHashes": x["sceneHashes"],
                "parcels": [
                    [int(y.split(",")[0]), int(y.split(",")[1])] for y in x["parcels"]
                ],
            }
            for x in latest_scene_list
        ]
        permissioned_worlds = [
            {
                "sceneName": x["sceneName"],
                "realm":x["realm"],
                "world":x["world"],
                "permissioned": True,
                "owned": False,
                "lastUpdate": x["updated"],
                "sceneHashes": x["sceneHashes"]
            }
            for x in worlds_scene_list            
        ]
    else:
        is_god_mode_user = False
        permissioned_land = [
            {
                "sceneName": x["sceneName"],
                "permissioned": True,
                "owned": False,
                "lastUpdate": x["updated"],
                "sceneHashes": x["sceneHashes"],
                "parcels": [
                    [int(y.split(",")[0]), int(y.split(",")[1])] for y in x["parcels"]
                ],
            }
            for x in latest_scene_list
            if any(
                aa in [x.lower() for x in x["approvedAccounts"]]
                for aa in [address.lower(), "everyone", "all", "*"]
            )
        ]
        permissioned_worlds = [
            {
                "sceneName": x["sceneName"],
                "realm":x["realm"],
                "world":x["world"],
                "permissioned": True,
                "owned": False,
                "lastUpdate": x["updated"],
                "sceneHashes": x["sceneHashes"]
            }
            for x in worlds_scene_list
            if any(
                aa in [x.lower() for x in x["approvedAccounts"]]
                for aa in [address.lower(), "everyone", "all", "*"]
            )            
        ]

    # get additional latest scenes from land/estate parcels
    all_parcels = [
        item
        for sublist in [
            [[int(y.split(",")[0]), int(y.split(",")[1])] for y in s["parcels"]]
            for s in latest_scene_list
        ]
        for item in sublist
    ]

    permissioned_parcels = [
        item
        for sublist in [s["parcels"] for s in permissioned_land]
        for item in sublist
    ]


    ### Genesis City Permissioning ###
    # owned, status, permissioned, name
    # plus a raw parcel list

    # if you own it...
    for p in owned_land["parcels"]:
        # and you're not already tagged in it...
        if p not in permissioned_parcels:
            # and we don't have data flowing on this parcel...
            if p not in all_parcels:
                # advise to hook up to our data
                permissioned_land.append(
                    {
                        "sceneName": "Untracked parcel " + str(p),
                        "lastUpdate": 0,
                        "owned": True,
                        "status": "Awaiting Atlas Analytics Deployment",
                        "parcels": [p],
                        "permission": False,
                    }
                )
            else:
                # owned but not tagged and reporting
                # permissioned_parcels.append(p)
                permissioned_land += [
                    {
                        "sceneName": x["_id"]["sceneName"],
                        "owned": True,
                        "lastUpdate": x["updated"],
                        "sceneHashes": x["sceneHashes"],
                        "parcels": [
                            [int(y.split(",")[0]), int(y.split(",")[1])]
                            for y in x["parcels"]
                        ],
                    }
                    for x in latest_scene_list
                    if p
                    in [
                        [int(y.split(",")[0]), int(y.split(",")[1])]
                        for y in x["parcels"]
                    ]
                ]
                permissioned_parcels += permissioned_land[-1]["parcels"]
        else:
            # add owned to permissioned
            for pl in permissioned_land:
                if p in pl["parcels"]:
                    pl["owned"] = True
                    break

    # repeat for estate parcels
    for e in owned_land["estates"]:
        for p in e["parcels"]:
            if p not in permissioned_parcels:
                if p not in all_parcels:
                    permissioned_land.append(
                        {
                            "sceneName": "Untracked parcel " + str(p),
                            "lastUpdate": 0,
                            "owned": True,
                            "status": "Awaiting Atlas Analytics Deployment",
                            "parcels": [p],
                            "permission": False,
                        }
                    )
                else:
                    # permissioned_parcels.append(p)
                    # print([{'sceneName':x['_id']['sceneName'],'lastUpdate':x['lastUpdate'],'parcels':[[int(y.split(",")[0]),int(y.split(",")[1])] for y in x['sceneInit']['sceneInitData']['parcels']]}
                    #                for x in latest_scene_list if 'sceneInitData' in x['sceneInit'] if 'parcels' in x['sceneInit']['sceneInitData']
                    #                    if p in [[int(y.split(",")[0]),int(y.split(",")[1])] for y in x['sceneInit']['sceneInitData']['parcels']]] ,flush=True)
                    permissioned_land += [
                        {
                            "sceneName": x["sceneName"],
                            "owned": True,
                            "lastUpdate": x["updated"],
                            "sceneHashes": x["sceneHashes"],
                            "parcels": [
                                [int(y.split(",")[0]), int(y.split(",")[1])]
                                for y in x["parcels"]
                            ],
                        }
                        for x in latest_scene_list
                        if p
                        in [
                            [int(y.split(",")[0]), int(y.split(",")[1])]
                            for y in x["parcels"]
                        ]
                    ]
                    permissioned_parcels += permissioned_land[-1]["parcels"]
            else:
                # add owned to permissioned
                for pl in permissioned_land:
                    if p in pl["parcels"]:
                        pl["owned"] = True
                        break

    for pl in permissioned_land:
        if "status" not in pl:
            if int(time.time() * 1000) - pl["lastUpdate"] > 28800000:
                pl["status"] = "Stale Signal"
            else:
                pl["status"] = "Online"

    # metadata addition
    for property in permissioned_land:
        latest_scene_list = [l for l in latest_scene_list if l["_id"] != {}]

        try:
            land_data = [
                l
                for l in latest_scene_list
                if property["sceneName"] == l["_id"]["sceneName"]
                if property["parcels"]
                == [[int(y.split(",")[0]), int(y.split(",")[1])] for y in l["parcels"]]
            ][0]
        except:
            pass

        try:
            property["favicon"] = land_data["sceneInit"]["sceneInitData"]["favicon"]
        except:
            property["favicon"] = ""

        try:
            property["description"] = land_data["sceneInit"]["sceneInitData"][
                "description"
            ]
        except:
            property["description"] = ""

        try:
            property["navmapThumbnail"] = land_data["sceneInit"]["sceneInitData"][
                "navmapThumbnail"
            ]
        except:
            property["navmapThumbnail"] = ""

    ### Worlds Permissioning ###
    # Owned names
    for name in names['names']:
        foundation_world = False
        atlas_world = False
        print(name,flush=True)
        deployed_names = [x for x in worlds_scene_list if getattr(x,'world','').lower()==name]
        for d in deployed_names:
            if d not in permissioned_worlds:
                permissioned_worlds.append(d)
            else:
                d['owned'] = True

            if d['realm'] == "atlas-worlds":
                atlas_world = True
            if d['realm'] == "foundation-worlds":
                foundation_world = True
        
        # Undeployed worlds for the two platforms that currently support them
        if not atlas_world:
            permissioned_worlds.append(
                    {
                        "sceneName": "Untracked world: Atlas Worlds " + str(name),
                        "realm":"atlas-worlds",
                        "world":name,
                        "lastUpdate": 0,
                        "owned": True,
                        "status": "Awaiting Atlas Analytics Deployment",
                        "permission": False,
                    }
                )
        
        if not foundation_world:
            permissioned_worlds.append(
                {
                    "sceneName": "Untracked world: Decentraland Worlds " + str(name),
                    "realm":"foundation-worlds",
                    "world":name,
                    "lastUpdate": 0,
                    "owned": True,
                    "status": "Awaiting Atlas Analytics Deployment",
                    "permission": False,
                }
            )

    for pw in permissioned_worlds:
        if "status" not in pw:
            if int(time.time() * 1000) - pw["lastUpdate"] > 28800000:
                pw["status"] = "Stale Signal"
            else:
                pw["status"] = "Online"

    if is_god_mode_user:
        data = {"scenes": permissioned_land, "worlds":permissioned_worlds,"god-mode": True}
    else:
        data = {"scenes": permissioned_land,"worlds":permissioned_worlds}

    print("scene-access complete " + str(time.time() - stopwatch), flush=True)
    if keep_in_house:
        return data["scenes"],data["worlds"]
    else:
        return jsonify(data)

def get_last_nonce(address):
    last = list(db.nonces.find({"address": address}).sort("nonce", -1).limit(1))[0]
    if "access_token" in last:
        return {"msg": "Already authenticated with this nonce."}, 400
    return last["nonce"]

@app.route("/get-nonce/<address>")
def get_nonce(address):
    nonce = int(time.time() * 1000)
    response = {"nonce": nonce, "address": address}
    db.nonces.insert_one(response)
    return jsonify(
        {
            "nonce": str(nonce)
            + "\n\nSigning this message will grant you access to Atlas Analytics for one hour.",
            "address": address,
        }
    )


@app.route("/authenticate", methods=["POST"])
def authenticate():
    if request.method == "POST":
        data = request.json
        try:
            signature = data["signature"]
        except:
            return {"msg": "No signature provided."}, 400
        try:
            address = data["address"]
        except:
            return {"msg": "No address provided."}, 400

    last_nonce = get_last_nonce(address)
    msg = {
        "address": address,
        "nonce": str(last_nonce)
        + "\n\nSigning this message will grant you access to Atlas Analytics for one hour.",
        "signature": signature,
    }

    # BROKEN - DOES NOT SUPPORT EIP 721 requiring an auth server (agh)
    # recovered_message = w3.eth.account.recover_message(encoded_message,signature=signature)

    recovered_message = requests.post(
        "http://analytics-express-auth:3000/recover-message",
        headers={"Content-Type": "application/json"},
        data=json.dumps(msg),
    )
    if recovered_message.text.lower() == address.lower():
        scenes,worlds = scene_access(address, True)
        if address.lower() in god_mode:
            access_token = create_access_token(
                identity={"address": address, "signature": signature},
                expires_delta=datetime.timedelta(hours=24),
                additional_claims={"address": address},
            )
            db.nonces.update_one(
                {"address": address, "nonce": last_nonce},
                {"$set": {"access_token": access_token, "scenes": scenes, "worlds":worlds}},
            )
            return {
                "auth": True,
                "access_token": access_token,
                "expiration": int(time.time() * 1000 + 3600000 * 24),
                "scenes": scenes,
                "worlds":worlds,
                "god-mode": True,
            }
        else:
            access_token = create_access_token(
                identity={"address": address, "signature": signature},
                expires_delta=datetime.timedelta(hours=1),
                additional_claims={"address": address},
            )
            db.nonces.update_one(
                {"address": address, "nonce": last_nonce},
                {"$set": {"access_token": access_token, "scenes": scenes,"worlds":worlds}},
            )
            return {
                "auth": True,
                "access_token": access_token,
                "expiration": int(time.time() * 1000 + 3600000),
                "scenes": scenes,
                "worlds":worlds
            }
    else:
        return {"auth": False}, 401


@app.route("/notifications", methods=["GET"])
@jwt_required()
def getNotifications():
    address = get_jwt_identity().get("address")

    data = list(
        db.notifications.aggregate(
            [
                {
                    "$lookup": {
                        "from": "notifications_views",
                        "let": {"yeet": "$_id"},
                        "pipeline": [
                            {
                                "$match": {
                                    "wallet": address,
                                    "$expr": {"$eq": ["$notification", "$$yeet"]},
                                    "read": True,
                                }
                            }
                        ],
                        "as": "swag",
                    }
                },
                {"$match": {"swag": {"$size": 0}}},
                {"$project": {"swag": 0}},
            ]
        )
    )

    return json_util.dumps(data), 200


@app.route("/notifications/read/<id>", methods=["GET"])
@jwt_required()
def readNotification(id):
    address = get_jwt_identity().get("address")
    if not address:
        return {"msg": "User does not have access to scene"}, 401

    db.notifications_views.update_one(
        {"notification": ObjectId(id), "wallet": address},
        {"$set": {"read": True}},
        True,
    )

    return "success", 200

@app.route("/notifications/submit", methods=["POST"])
@jwt_required()
def submitNotification():
    if not has_god_mode(get_jwt_identity().get("address")):
        return {"msg": "User does not have access to data"}, 401

    req = request.json

    required_keys = ["start", "end", "level", "title"]
    if not all(req.get(key) for key in required_keys):
        return "input incorrect", 403

    db.notifications.insert_one(
        {
            "start": req["start"],
            "end": req["end"],
            "text": req["title"],
            "level": req["level"],
        }
    )

    return "success", 200


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=port)
