import json
import os
from web3 import Web3
from web3.middleware import geth_poa_middleware
from dotenv import load_dotenv

load_dotenv(os.path.join(os.path.dirname(__file__), ".env"))

providerURL = os.environ.get("polygon_provider_url")

web3 = Web3(Web3.HTTPProvider(providerURL))
web3.middleware_onion.inject(geth_poa_middleware, layer=0)

wearables_abi_file = './wearables-abi.json'
with open(wearables_abi_file, 'r') as abi_definition:
    wearables_abi = json.load(abi_definition)

def emote_metadata(urn):

	collection_contract,item = urn.replace("urn:decentraland:matic:collections-v2:","").split(":")

	wearables_contract_address = Web3.toChecksumAddress(collection_contract.lower())
	contract = web3.eth.contract(address=wearables_contract_address, abi=wearables_abi)
	metadata = contract.functions.items(int(item)).call()

	return metadata