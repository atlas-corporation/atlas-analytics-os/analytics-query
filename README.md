# analytics-query

### A component of the Atlas Analytics platform

## About Atlas Analytics

Atlas Analytics is a platform for obtaining and reporting on analytics in the metaverse which rely on the Decentraland protocol. The platform was made open source as part of a [Decentraland grant](https://decentraland.org/governance/proposal/?id=fe85ab06-618d-4181-960d-fc32d5f0a7e1) and this repository constitues on of the microservices that comprises that platform.

The Atlas Analytics stack has the following components:

* `analytics-ingress` : Express application to receive analytics data from in-world. Contains filters and checks for users to apply if desired.

* `atlas-analytics-app`: The front-end, react application users log into to view reports on their analytics. 

* **YOU ARE HERE** `analytics-query` : The back-end of the analytics reporting application, providing queries into the database of collected data. Written in python.

* `analytics-express-auth` : A microservice for validating signatures due to functionality unsupported in python. Written in express.

* `analytics-update` : More back-end code, specifically for large queries that require caching periodically. Written in python.

* `analytics-update-cron` : A node-red application governing the periodic triggering of the `analytics-update` caching microservice.

*  `mongo-change-stream-server` : An event handler, written in express, to handle keeping a current index of active scenes and last updates.

* `mongo-change-stream-server-worlds` : An event handler, written in express, to handle keeping a current index of active scenes and last updates for worlds scenes.

## About analytics-query

This is the python flask application used to query the database using mongo aggregation pipelines. These endpoints serve as the back-end to the `atlas-analytics-app` react front end and use a standardized set of input POST body parameters which can be observed in the Postman collection committed with this repo. 

Many of the queries are designed to be flexible and use filters such that different switches and knobs in the front-end can be used with the same end points. Endpoints are unique to either genesis-city or worlds data and are contained in separate files.

This repository is the oldest part of the atlas-analytics stack and has been in development since February, 2021. While we believe many of the queries to utilize optimized aggregation pipelines (e.g. projecting before grouping) we encourage pull requests that would improve the performance of the platform.

## Deploying this Microservice

This microservice can be deployed using the dockerfile present in the repository. Simply run using `docker build -t analytics-query . && docker run analytics-query`. It should probably be run in concert with the `analytics-express-auth` microservice as it is meant to support that. In that case use `docker-compose build && docker-compose up -d` to execute.