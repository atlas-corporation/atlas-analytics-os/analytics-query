FROM python:3.9.16

WORKDIR /usr/src/app

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .

EXPOSE 4200

CMD ["python3","query.py"]
