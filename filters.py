import datetime
import os
from curses import flushinp

import dateutil.parser
import pymongo
from dotenv import load_dotenv
from database import client, db

# as of 4/20/2024
worlds_realms= {
    "atlas-worlds":"worlds-server.atlascorp.io/content/world/",
    "foundation-worlds":"",
    "decentral-games-worlds":"worlds.decentral.games/content/world/",
    "dcl-in-world-builder":"worlds.dcl-iwb.co/world/"
}

def parcels_in_scene(sceneName):
    last = list(
        db.analytics.find({"sceneName": sceneName, "eventName": "load-timer"})
        .sort("timestamp", -1)
        .limit(1)
    )[0]
    try:
        parcels = last["data"]["sceneInitData"]["parcels"]
        parcels = [[int(x.split(",")[0]), int(x.split(",")[1])] for x in parcels]
    except:
        parcels = []
    return parcels

def scene_filter(req):
    if req:
        if "scene" in req:
            scene_filter = [{"$match": {"sceneName": req["scene"]}}]
            if "include-scene-bleed" in req:
                if req["include-scene-bleed"]:
                    pass
                else:
                    # only parcels in scene - get from latest load-time filtered by timestamp
                    if "parcels" in req:
                        scene_filter = [
                            {
                                "$match": {
                                    "sceneName": req["scene"],
                                    "parcel": {"$in": req["parcels"]},
                                }
                            }
                        ]
                    else:
                        scene_filter = [
                            {
                                "$match": {
                                    "sceneName": req["scene"],
                                    "parcel": {"$in": parcels_in_scene(req["scene"])},
                                }
                            }
                        ]
            else:
                if "parcels" in req:
                    scene_filter = [
                        {
                            "$match": {
                                "sceneName": req["scene"],
                                "parcel": {"$in": req["parcels"]},
                            }
                        }
                    ]
                else:
                    scene_filter = [
                        {
                            "$match": {
                                "sceneName": req["scene"],
                                "parcel": {"$in": parcels_in_scene(req["scene"])},
                            }
                        }
                    ]
        else:
            scene_filter = []

    else:
        scene_filter = []

    return scene_filter

def branch_filter(req):
    scene_filter = []
    if "branch" in req:
        if req["branch"] and req["branch"] != "":
            scene_filter += [{"$match": {"sceneBranch": req["branch"]}}]
    return scene_filter

def realm_filter(req):
    scene_filter = []
    if "realm" in req:
        if req["realm"] and req["realm"] != "":
            scene_filter += [{"$match": {"realm": req["realm"]}}]
    return scene_filter

def bin_size(req):
    if req:
        if "binSize" in req:
            return int(req["binSize"])
        else:
            return 5
    else:
        return 5

def timezoneOffset(req):
    if req and "timezoneOffset" in req:
        tz = int(req["timezoneOffset"]) / -60
        if tz < 10 and tz > 0:
            tz = "+0" + str(abs(int(tz)))
        elif tz > -10 and tz < 0:
            tz = "-0" + str(abs(int(tz)))
        elif tz == 0:
            return "+00"
        return tz
    else:
        return "+00"

# Assumes user inputs ISO date format (so they dont have to look up timestamps)
def time_bands(req):
    if req:
        if "start" in req and "end" in req:
            start = dateutil.parser.isoparse(req["start"]).timestamp() * 1000
            end = dateutil.parser.isoparse(req["end"]).timestamp() * 1000
            timeband = [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {"$gte": ["$timestamp", start]},
                                {"$lte": ["$timestamp", end]},
                            ]
                        }
                    }
                }
            ]
        elif "start" in req:
            start = dateutil.parser.isoparse(req["start"]).timestamp() * 1000
            timeband = [{"$match": {"$expr": {"$gte": ["$timestamp", start]}}}]
        elif "end" in req:
            end = dateutil.parser.isoparse(req["end"]).timestamp() * 1000
            timeband = [{"$match": {"$expr": {"$lte": ["$timestamp", end]}}}]
        else:
            timeband = []

        return timeband
    else:
        return []

def include_users(req):
    projection = [
        {
            "$project": {
                "unique-users": 0,
                "households": 0,
                "wallets": 0,
                "hephaestus-users": 0,
                "hela-users": 0,
                "heimdallr-users": 0,
                "baldr-users": 0,
                "artemis-users": 0,
                "loki-users": 0,
                "dg-users": 0,
                "odin-users": 0,
                "unicorn-users": 0,
                "marvel-users": 0,
                "athena-users": 0,
            }
        }
    ]
    if req:
        if "include-users" in req:
            if req["include-users"]:
                projection = []
    return projection

def parcel_filter(req, outsideIn=False):
    match = []
    if req:
        if ("include-scene-bleed" in req and not req["include-scene-bleed"]) or (
            "include-scene-bleed" not in req
        ):
            if outsideIn:
                if "parcel" in req:
                    match = [
                        {"$match": {"payload.islands.peers.parcel": req["parcel"]}}
                    ]
                elif "parcels" in req:
                    match = [
                        {
                            "$match": {
                                "payload.islands.peers.parcel": {"$in": req["parcels"]}
                            }
                        }
                    ]
            else:
                if "parcel" in req:
                    match = [{"$match": {"parcel": req["parcel"]}}]
                elif "parcels" in req:
                    match = [{"$match": {"parcel": {"$in": req["parcels"]}}}]

    return match

def pad_zeroes(req, time_series, bin):
    padded_time_series = time_series
    if req and len(time_series):
        if "zero-padded" in req and req["zero-padded"]:
            padded_time_series = []
            if "start" in req:
                start = dateutil.parser.isoparse(req["start"])
            else:
                start = time_series[0]["_id"].replace(tzinfo=datetime.timezone.utc)
            if "end" in req:
                end = dateutil.parser.isoparse(req["end"])
            else:
                end = time_series[-1]["_id"].replace(tzinfo=datetime.timezone.utc)
            for i in range(
                int(datetime.datetime.timestamp(start)),
                int(datetime.datetime.timestamp(end)) + 1,
                bin,
            ):
                # replace is so that the mongodb datetime object is converted to UTC time instead of local time
                t = [
                    x
                    for x in time_series
                    if (
                        int(
                            datetime.datetime.timestamp(
                                x["_id"].replace(tzinfo=datetime.timezone.utc)
                            )
                        )
                        >= i - bin / 2
                        and int(
                            datetime.datetime.timestamp(
                                x["_id"].replace(tzinfo=datetime.timezone.utc)
                            )
                        )
                        < i + bin / 2
                    )
                ]
                if len(t) > 0:
                    padded_time_series.append(t[0])
                else:
                    padded_time_series.append(
                        {
                            "_id": datetime.datetime.fromtimestamp(i).astimezone(
                                datetime.timezone.utc
                            ),
                            "count": 0,
                        }
                    )

    return padded_time_series

def pad_zeroes_realms(req, time_series, bin=86400):
    padded_time_series = time_series
    if req:
        if "zero-padded" in req and req["zero-padded"]:
            padded_time_series = []
            if "start" in req:
                start = dateutil.parser.isoparse(req["start"])
            else:
                start = time_series[0]["_id"]
            if "end" in req:
                end = dateutil.parser.isoparse(req["end"])
            else:
                end = time_series[-1]["_id"]

            realms = set().union(*(d.keys() for d in time_series))
            realms = [x for x in realms if x != "_id"]
            print(realms, flush=True)
            for i in range(
                int(datetime.datetime.timestamp(start)),
                int(datetime.datetime.timestamp(end)),
                bin,
            ):
                t = [
                    x
                    for x in time_series
                    if (
                        int(
                            datetime.datetime.timestamp(
                                x["_id"].replace(tzinfo=datetime.timezone.utc)
                            )
                        )
                        >= i - bin / 2
                        and int(
                            datetime.datetime.timestamp(
                                x["_id"].replace(tzinfo=datetime.timezone.utc)
                            )
                        )
                        < i + bin / 2
                    )
                ]

                if len(t) > 0:
                    for r in realms:
                        if r not in t[0]:
                            t[0][r] = 0
                    padded_time_series.append(t[0])
                else:
                    ts = {
                        "_id": datetime.datetime.fromtimestamp(i).astimezone(
                            datetime.timezone.utc
                        )
                    }
                    for r in realms:
                        ts[r] = 0
                    padded_time_series.append(ts)
    return padded_time_series

def get_server():
    return "catalyst.atlascorp.io"

def matchForMongo(req,timeless=False):
    matchDict = {}
    if req:
        if "scene" in req:
            matchDict["sceneName"] = req["scene"]
            if "include-scene-bleed" in req:
                if not req["include-scene-bleed"]:
                    if "parcels" in req:
                        matchDict["parcel"] = {"$in": req["parcels"]}
                    else:
                        if 'world' not in req:
                            matchDict["parcel"] = {"$in": parcels_in_scene(req["scene"])}
            else:
                if "parcels" in req:
                    matchDict["parcel"] = {"$in": req["parcels"]}
                else:
                    if 'world' not in req:
                        matchDict["parcel"] = {"$in": parcels_in_scene(req["scene"])}
        
        #stickiness calcs handle their own time windowing
        if not timeless:
            if "start" in req and "end" in req:
                start = dateutil.parser.isoparse(req["start"]).timestamp() * 1000
                end = dateutil.parser.isoparse(req["end"]).timestamp() * 1000
                matchDict["timestamp"] = {"$gte": start, "$lte": end}
            elif "start" in req:
                start = dateutil.parser.isoparse(req["start"]).timestamp() * 1000
                matchDict["timestamp"] = {"$gte": start}
            elif "end" in req:
                end = dateutil.parser.isoparse(req["end"]).timestamp() * 1000
                matchDict["timestamp"] = {"$lte": end}
                
        if "branch" in req:
            if req["branch"] != "":
                matchDict["sceneBranch"] = req["branch"]
        if "realm" in req:
            if req["realm"] != "":
                matchDict["realm"] = req["realm"]
        if "world" in req:
            if req["world"] != "":
                try:
                    matchDict["realm"] = worlds_realms[req["realm"]] + req["world"]
                except:
                    pass
        if len(matchDict):
            return [{"$match": matchDict}]
    return []

def matchForMongoHyperfy(req):
    matchDict = {}
    if req:
        if "apikey" in req:
            matchDict["apikey"] = req["apikey"]
        if "worldId" in req:
            matchDict["worldId"] = req["worldId"]
        if "start" in req and "end" in req:
            start = dateutil.parser.isoparse(req["start"]).timestamp() * 1000
            end = dateutil.parser.isoparse(req["end"]).timestamp() * 1000
            matchDict["timestamp"] = {"$gte": start, "$lte": end}
        elif "start" in req:
            start = dateutil.parser.isoparse(req["start"]).timestamp() * 1000
            matchDict["timestamp"] = {"$gte": start}
        elif "end" in req:
            end = dateutil.parser.isoparse(req["end"]).timestamp() * 1000
            matchDict["timestamp"] = {"$lte": end}
        if "branch" in req:
            if req["branch"] != "":
                matchDict["sceneBranch"] = req["branch"]
        if len(matchDict):
            return [{"$match": matchDict}]
    return []
